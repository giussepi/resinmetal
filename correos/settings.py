# -*- coding: utf-8 -*-

""" Settings of Correos """

from django.conf import settings
from os import path

#GETTING VALUES DEFINED IN PROJECT SETTINGS
ATTACHMENTS_FOLDER = path.join(
    'correos',
    getattr(settings, 'ATTACHMENTS_FOLDER_NAME', 'attachments'), )
