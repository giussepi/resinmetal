# -*- coding: utf-8 -*-
""" correos forms """

from django import forms
from correos.models import Correo


class CorreoForm(forms.ModelForm):
    """ correo form """

    class Meta:
        model = Correo
