# -*- coding: utf-8 -*-
""" Models Correos """

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from common.utils import highly_random_filename, send_mass_html_mails
from common.validators import validate_multiple_emails
import correos.settings as correos_settings


def size_validator(myfile):
    """
    file size < 5Mb
    """
    if myfile.size > 5000000:
        raise ValidationError(
            u"Your photo must not be larger than 5 megabytes in size.")


class Correo(models.Model):
    """ stores the emails """
    correos = models.CharField(max_length=250,
                               validators=[validate_multiple_emails])
    asunto = models.CharField(max_length=120)
    texto = models.TextField(u'Mensaje')

    def get_path(self, filename):
        """ returns the path and filename """
        return u'%s/%s' % (correos_settings.ATTACHMENTS_FOLDER,
                           highly_random_filename(filename))

    archivo_adjunto = models.ImageField(
        upload_to=get_path, max_length=200, validators=[size_validator],
        help_text=u'El nombre del archivo puede tener hasta 200 caractéres.',
        blank=True, null=True)
    fecha_envio = models.DateTimeField(auto_now_add=True)
    user_sender = models.ForeignKey(User)
    proyecto = models.ForeignKey('proyectos.Proyecto', blank=True, null=True)
    # esto seria un contrato en especifico que se envia x correo
    # si no se encuentra un uso realmente necesario o util hay q borrarlo
    contrato = models.ForeignKey('proyectos.Contrato', blank=True, null=True)
    inclusionexclusion = models.ForeignKey('proyectos.InclusionExclusion',
                                           blank=True, null=True,
                                           verbose_name=u'inclusión exclusión')

    class Meta:
        ordering = ['-fecha_envio', ]

    def __unicode__(self):
        return u'%s' % self.asunto


def send_email(sender, instance, created, **kwargs):
    """ sends the email only when object is created """
    correo = instance
    if created:
        try:
            attachments = [correo.archivo_adjunto.path]
        except ValueError:
            attachments = []
        send_mass_html_mails(
            correo.user_sender.email, correo.asunto,
            'simple_email.html',
            {'message': correo.texto,
             'sender_name': correo.user_sender.get_full_name(),
             'STATIC_URL': settings.STATIC_URL,
             'sitio': Site.objects.get(id=1), },
            correo.correos.split(','), correo.texto,
            attachments or []
        )
post_save.connect(send_email, sender=Correo)
