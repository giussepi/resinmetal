# -*- coding: utf-8 -*-
""" Correos Admin """

from django.contrib import admin
from models import Correo


class CorreoAdmin(admin.ModelAdmin):
    """ admin model that manages the Emails ('Correos') """

    list_display = ('__unicode__', 'proyecto', 'inclusionexclusion',
                    'fecha_envio')
    list_display_links = ('__unicode__', 'proyecto', 'inclusionexclusion',
                          'fecha_envio')
    date_hierarchy = 'fecha_envio'

admin.site.register(Correo, CorreoAdmin)
