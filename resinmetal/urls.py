""" resinmetal urls """

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import RedirectView


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'resinmetal.views.home', name='home'),
    # url(r'^resinmetal/', include('resinmetal.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^$', RedirectView.as_view(url='admin/')),
    url(r'^admin/', include(admin.site.urls)),

    #django tinymce
    url(r'^tinymce/', include('tinymce.urls')),

    # Media & Static
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
)
