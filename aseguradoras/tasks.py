# -*- coding: utf-8 -*-

""" aseguradoras tasks """

from celery import task
from aseguradoras.models import Poliza
from datetime import date


@task()
def validate_insurances():
    """ updates the insurances validity """
    Poliza.objects.filter(fecha_fin_vigencia__lt=date.today).update(
        vigente=False)
    Poliza.objects.filter(fecha_fin_vigencia__gte=date.today).update(
        vigente=True)
    return 'Polizas actualizadas'
