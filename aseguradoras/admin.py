# -*- coding: utf-8 -*-
""" Aseguradoras Admin """

from django.contrib import admin
from aseguradoras.models import Aseguradora, Encargado, Poliza


class AseguradoraAdmin(admin.ModelAdmin):
    """
    admin model that manages the insurances companies ('aseguradoras') objects
    """
    fieldsets = (
        (u'datos básicos', {
            'fields': ('nombre', 'valoracion')
        }),
        (u'otros datos', {
            'classes': ('collapse', ),
            'fields': ('ruc', 'email', 'web', )
        }),
    )
    exclude = ('telefonos', 'observaciones', )
    search_fields = ('nombre', )


class EncargadoAdmin(admin.ModelAdmin):
    """
    admin model that manages the insurance agent ('encargado') objects
    """
    list_display = ('apellidos', 'nombres', 'aseguradora', 'vigente')
    list_display_links = ('apellidos', 'nombres', 'aseguradora', 'vigente')
    fieldsets = (
        (u'datos básicos', {
            'fields': ('aseguradora', 'nombres', 'apellidos', 'email', 'sexo',
                       'valoracion', 'activo')
        }),
        (u'otros datos', {
            'classes': ('collapse', ),
            'fields': ('dni', 'ruc', 'web')
        }),
    )
    exclude = ('telefonos', 'observaciones', )
    search_fields = ('apellidos', )
    list_filter = ('aseguradora', )


class PolizaAdmin(admin.ModelAdmin):
    """
    admin model that manages the insurance ('poliza') objects
    """
    list_display = ('numero', 'aseguradora', 'vigencia', )
    list_display_links = ('numero', 'aseguradora', 'vigencia', )
    list_filter = ('aseguradora', )
    search_fields = ('^numero', )

admin.site.register(Aseguradora, AseguradoraAdmin)
admin.site.register(Encargado, EncargadoAdmin)
admin.site.register(Poliza, PolizaAdmin)
