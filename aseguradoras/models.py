# -*- coding: utf-8 -*-
""" Aseguradoras Models """

from django.db import models
from django.conf import settings
from datetime import date
from common.models import BaseData


class Aseguradora(BaseData):
    """ stores the insurances companies """
    nombre = models.CharField(max_length=120)
    email = models.EmailField(u'Correo', blank=True, null=True)

    def __unicode__(self):
        return u'%s' % self.nombre


class Encargado(BaseData):
    """ stotes insurances company agents """
    SEXO = ((u'M', u'Masculino'),
            (u'F', u'Femenino'),
            )
    nombres = models.CharField(max_length=120)
    apellidos = models.CharField(max_length=120)
    email = models.EmailField(u'Correo', null=True)
    sexo = models.CharField(max_length=1, choices=SEXO)
    activo = models.BooleanField(default=True)
    dni = models.CharField(max_length=8, blank=True)
    aseguradora = models.ForeignKey(Aseguradora)

    def __unicode__(self):
        return u'%s %s (%s) ' % (
            self.nombres, self.apellidos, self.aseguradora)

    def vigente(self):
        """
        returns the html code that indicates if the agent is active or not
        """
        if self.activo:
            icon = u'yes'
        else:
            icon = u'no'
        return u'<img src="%sadmin/img/icon-%s.gif" alt="activo">' % (
            settings.STATIC_URL, icon)
    vigente.allow_tags = True
    vigente.short_description = u'en actividad'


class Poliza(models.Model):
    """ stores de insurances """
    numero = models.CharField(max_length=20)
    vigente = models.BooleanField(default=True)
    fecha_inicio_vigencia = models.DateField()
    fecha_fin_vigencia = models.DateField(blank=True, null=True)
    aseguradora = models.ForeignKey(Aseguradora)

    def __unicode__(self):
        return u'%s (%s)' % (self.numero, self.aseguradora)

    def save(self, *args, **kwargs):
        """
        if the insurance end date is less than actual date, the attribute
        'vigente' is set to False, otherwise is set to True.
        """
        if self.fecha_fin_vigencia < date.today():
            self.vigente = False
        else:
            self.vigente = True
        super(self.__class__, self).save(*args, **kwargs)

    def vigencia(self):
        """
        returns the html code that indicates if the insurance is valir or not
        """
        if self.vigente:
            icon = u'yes'
        else:
            icon = u'no'
        return u'<img src="%sadmin/img/icon-%s.gif" alt="activo">' % (
            settings.STATIC_URL, icon)
    vigencia.allow_tags = True
    vigencia.short_description = u'Vigencia'
