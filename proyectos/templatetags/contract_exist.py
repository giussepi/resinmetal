# -*- coding: utf-8 -*-
""" proyectos template filters """

from django import template
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from proyectos.models import Proyecto
import os


register = template.Library()


@register.assignment_tag
def contract_exist(project_id):
    """  """
    try:
        project = Proyecto.objects.get(id=project_id)
    except ObjectDoesNotExist:
        return False
    else:
        return project.contrato_set.exists()
