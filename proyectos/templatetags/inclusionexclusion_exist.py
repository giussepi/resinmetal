# -*- coding: utf-8 -*-
""" proyectos template filters """

from django import template
from django.conf import settings
from proyectos import settings as proyectos_settings
import os


register = template.Library()


@register.assignment_tag
def inclusionexclusion_exist(project_id, inclusionexclusion_id):
    """  """
    path = os.path.join(settings.MEDIA_ROOT,
                        proyectos_settings.CONTRATOS_PDF_FOLDER,
                        'contrato_proyecto_%s_inclusionexclusion_%d.pdf' % (
                            project_id, inclusionexclusion_id))
    if os.path.exists(path):
        return True
    return False
