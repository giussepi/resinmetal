# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Contrato.pdf'
        db.delete_column('proyectos_contrato', 'pdf')


    def backwards(self, orm):
        # Adding field 'Contrato.pdf'
        db.add_column('proyectos_contrato', 'pdf',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=150, blank=True),
                      keep_default=False)


    models = {
        'aseguradoras.aseguradora': {
            'Meta': {'object_name': 'Aseguradora'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'observaciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'aseguradoras_aseguradora_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Observacion']"}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'aseguradoras_aseguradora_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Telefono']"}),
            'valoracion': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'aseguradoras.encargado': {
            'Meta': {'object_name': 'Encargado'},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'aseguradora': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aseguradoras.Aseguradora']"}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombres': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'observaciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'aseguradoras_encargado_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Observacion']"}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'telefonos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'aseguradoras_encargado_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Telefono']"}),
            'valoracion': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'aseguradoras.poliza': {
            'Meta': {'object_name': 'Poliza'},
            'aseguradora': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aseguradoras.Aseguradora']"}),
            'fecha_fin_vigencia': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fecha_inicio_vigencia': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'vigente': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'clientes.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'direcciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'clientes_cliente_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Direccion']"}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'grado_instruccion': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'observaciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'clientes_cliente_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Observacion']"}),
            'ocupacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'default': "u'M'", 'max_length': '1'}),
            'telefonos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'clientes_cliente_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Telefono']"}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'valoracion': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'common.departamento': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Departamento'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'common.direccion': {
            'Meta': {'object_name': 'Direccion'},
            'departamento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Departamento']"}),
            'distrito': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Distrito']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'provincia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Provincia']"}),
            'texto': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'common.distrito': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Distrito'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'provincia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Provincia']"})
        },
        'common.observacion': {
            'Meta': {'ordering': "['-fecha']", 'object_name': 'Observacion'},
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.IntegerField', [], {})
        },
        'common.provincia': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Provincia'},
            'departamento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Departamento']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'common.telefono': {
            'Meta': {'object_name': 'Telefono'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.TipoTelefono']"})
        },
        'common.tipotelefono': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'TipoTelefono'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'personal.especialidad': {
            'Meta': {'object_name': 'Especialidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'personal.personal': {
            'Meta': {'object_name': 'Personal'},
            'antecedentes_policiales': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'direcciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'personal_personal_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Direccion']"}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'especialidades': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['personal.Especialidad']", 'through': "orm['personal.PersonalEspecialidad']", 'symmetrical': 'False'}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "u'viable'", 'max_length': '12'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'grado_instruccion': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'grupo_sanguineo': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nro_cuspp': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'nro_seguro_social': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'observaciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'personal_personal_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Observacion']"}),
            'ocupacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'default': "u'M'", 'max_length': '1'}),
            'sistema_de_pensiones': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'personal_personal_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Telefono']"}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "u'2'", 'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'valoracion': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'personal.personalespecialidad': {
            'Meta': {'object_name': 'PersonalEspecialidad'},
            'especialidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['personal.Especialidad']"}),
            'experiencia': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['personal.Personal']"})
        },
        'proyectos.contrato': {
            'Meta': {'ordering': "['-fecha_inicio', 'proyecto']", 'object_name': 'Contrato'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['clientes.Cliente']", 'null': 'True', 'blank': 'True'}),
            'contrato_escaneado': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fecha_fin': ('django.db.models.fields.DateField', [], {}),
            'fecha_inicio': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inclusionesexclusiones': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['proyectos.InclusionExclusion']", 'null': 'True', 'blank': 'True'}),
            'monto': ('django.db.models.fields.FloatField', [], {}),
            'personal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['personal.Personal']", 'null': 'True', 'blank': 'True'}),
            'polizas': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['aseguradoras.Poliza']", 'null': 'True', 'blank': 'True'}),
            'proyecto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['proyectos.Proyecto']"}),
            'texto': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'tipo_inclusionexclusion': ('django.db.models.fields.CharField', [], {'default': "u'n'", 'max_length': '1', 'blank': 'True'})
        },
        'proyectos.inclusionexclusion': {
            'Meta': {'ordering': "['-fecha']", 'object_name': 'InclusionExclusion'},
            'encargados': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['aseguradoras.Encargado']", 'symmetrical': 'False'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fecha_inicio': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personal_excluido': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'exclusion_set'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['personal.Personal']"}),
            'personal_incluido': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'inclusion_set'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['personal.Personal']"}),
            'proyecto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['proyectos.Proyecto']"})
        },
        'proyectos.modelocontrato': {
            'Meta': {'ordering': "['titulo']", 'object_name': 'ModeloContrato'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'proyectos.proyecto': {
            'Meta': {'ordering': "['-fecha_inicio']", 'object_name': 'Proyecto'},
            'aseguradoras': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['aseguradoras.Aseguradora']", 'symmetrical': 'False'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['clientes.Cliente']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'encargados': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['aseguradoras.Encargado']", 'symmetrical': 'False'}),
            'fecha_fin': ('django.db.models.fields.DateField', [], {}),
            'fecha_inicio': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.FloatField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'personal': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['personal.Personal']", 'symmetrical': 'False'})
        }
    }

    complete_apps = ['proyectos']