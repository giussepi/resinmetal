# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ModeloContrato'
        db.create_table('proyectos_modelocontrato', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('texto', self.gf('django.db.models.fields.TextField')()),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('proyectos', ['ModeloContrato'])

        # Adding model 'Proyecto'
        db.create_table('proyectos_proyecto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('descripcion', self.gf('django.db.models.fields.TextField')()),
            ('fecha_inicio', self.gf('django.db.models.fields.DateField')()),
            ('fecha_fin', self.gf('django.db.models.fields.DateField')()),
            ('monto', self.gf('django.db.models.fields.FloatField')()),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['clientes.Cliente'])),
        ))
        db.send_create_signal('proyectos', ['Proyecto'])

        # Adding M2M table for field personal on 'Proyecto'
        db.create_table('proyectos_proyecto_personal', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('proyecto', models.ForeignKey(orm['proyectos.proyecto'], null=False)),
            ('personal', models.ForeignKey(orm['personal.personal'], null=False))
        ))
        db.create_unique('proyectos_proyecto_personal', ['proyecto_id', 'personal_id'])

        # Adding M2M table for field encargados on 'Proyecto'
        db.create_table('proyectos_proyecto_encargados', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('proyecto', models.ForeignKey(orm['proyectos.proyecto'], null=False)),
            ('encargado', models.ForeignKey(orm['aseguradoras.encargado'], null=False))
        ))
        db.create_unique('proyectos_proyecto_encargados', ['proyecto_id', 'encargado_id'])

        # Adding M2M table for field aseguradoras on 'Proyecto'
        db.create_table('proyectos_proyecto_aseguradoras', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('proyecto', models.ForeignKey(orm['proyectos.proyecto'], null=False)),
            ('aseguradora', models.ForeignKey(orm['aseguradoras.aseguradora'], null=False))
        ))
        db.create_unique('proyectos_proyecto_aseguradoras', ['proyecto_id', 'aseguradora_id'])

        # Adding model 'InclusionExclusion'
        db.create_table('proyectos_inclusionexclusion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('fecha_inicio', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('proyecto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['proyectos.Proyecto'])),
        ))
        db.send_create_signal('proyectos', ['InclusionExclusion'])

        # Adding M2M table for field personal_incluido on 'InclusionExclusion'
        db.create_table('proyectos_inclusionexclusion_personal_incluido', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('inclusionexclusion', models.ForeignKey(orm['proyectos.inclusionexclusion'], null=False)),
            ('personal', models.ForeignKey(orm['personal.personal'], null=False))
        ))
        db.create_unique('proyectos_inclusionexclusion_personal_incluido', ['inclusionexclusion_id', 'personal_id'])

        # Adding M2M table for field personal_excluido on 'InclusionExclusion'
        db.create_table('proyectos_inclusionexclusion_personal_excluido', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('inclusionexclusion', models.ForeignKey(orm['proyectos.inclusionexclusion'], null=False)),
            ('personal', models.ForeignKey(orm['personal.personal'], null=False))
        ))
        db.create_unique('proyectos_inclusionexclusion_personal_excluido', ['inclusionexclusion_id', 'personal_id'])

        # Adding M2M table for field encargados on 'InclusionExclusion'
        db.create_table('proyectos_inclusionexclusion_encargados', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('inclusionexclusion', models.ForeignKey(orm['proyectos.inclusionexclusion'], null=False)),
            ('encargado', models.ForeignKey(orm['aseguradoras.encargado'], null=False))
        ))
        db.create_unique('proyectos_inclusionexclusion_encargados', ['inclusionexclusion_id', 'encargado_id'])

        # Adding model 'Contrato'
        db.create_table('proyectos_contrato', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha_inicio', self.gf('django.db.models.fields.DateField')()),
            ('fecha_fin', self.gf('django.db.models.fields.DateField')()),
            ('monto', self.gf('django.db.models.fields.FloatField')()),
            ('texto', self.gf('django.db.models.fields.TextField')()),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('contrato_escaneado', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('pdf', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('tipo_inclusionexclusion', self.gf('django.db.models.fields.CharField')(default=u'n', max_length=1, blank=True)),
            ('proyecto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['proyectos.Proyecto'])),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['clientes.Cliente'], null=True, blank=True)),
            ('personal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['personal.Personal'], null=True, blank=True)),
        ))
        db.send_create_signal('proyectos', ['Contrato'])

        # Adding M2M table for field polizas on 'Contrato'
        db.create_table('proyectos_contrato_polizas', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('contrato', models.ForeignKey(orm['proyectos.contrato'], null=False)),
            ('poliza', models.ForeignKey(orm['aseguradoras.poliza'], null=False))
        ))
        db.create_unique('proyectos_contrato_polizas', ['contrato_id', 'poliza_id'])

        # Adding M2M table for field inclusionesexclusiones on 'Contrato'
        db.create_table('proyectos_contrato_inclusionesexclusiones', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('contrato', models.ForeignKey(orm['proyectos.contrato'], null=False)),
            ('inclusionexclusion', models.ForeignKey(orm['proyectos.inclusionexclusion'], null=False))
        ))
        db.create_unique('proyectos_contrato_inclusionesexclusiones', ['contrato_id', 'inclusionexclusion_id'])


    def backwards(self, orm):
        # Deleting model 'ModeloContrato'
        db.delete_table('proyectos_modelocontrato')

        # Deleting model 'Proyecto'
        db.delete_table('proyectos_proyecto')

        # Removing M2M table for field personal on 'Proyecto'
        db.delete_table('proyectos_proyecto_personal')

        # Removing M2M table for field encargados on 'Proyecto'
        db.delete_table('proyectos_proyecto_encargados')

        # Removing M2M table for field aseguradoras on 'Proyecto'
        db.delete_table('proyectos_proyecto_aseguradoras')

        # Deleting model 'InclusionExclusion'
        db.delete_table('proyectos_inclusionexclusion')

        # Removing M2M table for field personal_incluido on 'InclusionExclusion'
        db.delete_table('proyectos_inclusionexclusion_personal_incluido')

        # Removing M2M table for field personal_excluido on 'InclusionExclusion'
        db.delete_table('proyectos_inclusionexclusion_personal_excluido')

        # Removing M2M table for field encargados on 'InclusionExclusion'
        db.delete_table('proyectos_inclusionexclusion_encargados')

        # Deleting model 'Contrato'
        db.delete_table('proyectos_contrato')

        # Removing M2M table for field polizas on 'Contrato'
        db.delete_table('proyectos_contrato_polizas')

        # Removing M2M table for field inclusionesexclusiones on 'Contrato'
        db.delete_table('proyectos_contrato_inclusionesexclusiones')


    models = {
        'aseguradoras.aseguradora': {
            'Meta': {'object_name': 'Aseguradora'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'observaciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'aseguradoras_aseguradora_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Observacion']"}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'aseguradoras_aseguradora_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Telefono']"}),
            'valoracion': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'aseguradoras.encargado': {
            'Meta': {'object_name': 'Encargado'},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'apellidos': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'aseguradora': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aseguradoras.Aseguradora']"}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombres': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'observaciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'aseguradoras_encargado_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Observacion']"}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'telefonos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'aseguradoras_encargado_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Telefono']"}),
            'valoracion': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'aseguradoras.poliza': {
            'Meta': {'object_name': 'Poliza'},
            'aseguradora': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aseguradoras.Aseguradora']"}),
            'fecha_fin_vigencia': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fecha_inicio_vigencia': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'vigente': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'clientes.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'direcciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'clientes_cliente_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Direccion']"}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'grado_instruccion': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'observaciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'clientes_cliente_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Observacion']"}),
            'ocupacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'default': "u'M'", 'max_length': '1'}),
            'telefonos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'clientes_cliente_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Telefono']"}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'valoracion': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'common.departamento': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Departamento'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'common.direccion': {
            'Meta': {'object_name': 'Direccion'},
            'departamento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Departamento']"}),
            'distrito': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Distrito']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'provincia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Provincia']"}),
            'texto': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'common.distrito': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Distrito'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'provincia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Provincia']"})
        },
        'common.observacion': {
            'Meta': {'ordering': "['-fecha']", 'object_name': 'Observacion'},
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.IntegerField', [], {})
        },
        'common.provincia': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Provincia'},
            'departamento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Departamento']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'common.telefono': {
            'Meta': {'object_name': 'Telefono'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.TipoTelefono']"})
        },
        'common.tipotelefono': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'TipoTelefono'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'personal.especialidad': {
            'Meta': {'object_name': 'Especialidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'personal.personal': {
            'Meta': {'object_name': 'Personal'},
            'antecedentes_policiales': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'direcciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'personal_personal_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Direccion']"}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'especialidades': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['personal.Especialidad']", 'through': "orm['personal.PersonalEspecialidad']", 'symmetrical': 'False'}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "u'viable'", 'max_length': '12'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'grado_instruccion': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'grupo_sanguineo': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nro_cuspp': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'nro_seguro_social': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'observaciones': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'personal_personal_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Observacion']"}),
            'ocupacion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ruc': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'default': "u'M'", 'max_length': '1'}),
            'sistema_de_pensiones': ('django.db.models.fields.CharField', [], {'max_length': '3', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'personal_personal_related'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['common.Telefono']"}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "u'2'", 'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'valoracion': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'personal.personalespecialidad': {
            'Meta': {'object_name': 'PersonalEspecialidad'},
            'especialidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['personal.Especialidad']"}),
            'experiencia': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['personal.Personal']"})
        },
        'proyectos.contrato': {
            'Meta': {'ordering': "['-fecha_inicio', 'proyecto']", 'object_name': 'Contrato'},
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['clientes.Cliente']", 'null': 'True', 'blank': 'True'}),
            'contrato_escaneado': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'fecha_fin': ('django.db.models.fields.DateField', [], {}),
            'fecha_inicio': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inclusionesexclusiones': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['proyectos.InclusionExclusion']", 'null': 'True', 'blank': 'True'}),
            'monto': ('django.db.models.fields.FloatField', [], {}),
            'pdf': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'personal': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['personal.Personal']", 'null': 'True', 'blank': 'True'}),
            'polizas': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['aseguradoras.Poliza']", 'null': 'True', 'blank': 'True'}),
            'proyecto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['proyectos.Proyecto']"}),
            'texto': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'tipo_inclusionexclusion': ('django.db.models.fields.CharField', [], {'default': "u'n'", 'max_length': '1', 'blank': 'True'})
        },
        'proyectos.inclusionexclusion': {
            'Meta': {'ordering': "['-fecha']", 'object_name': 'InclusionExclusion'},
            'encargados': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['aseguradoras.Encargado']", 'symmetrical': 'False'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fecha_inicio': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personal_excluido': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'exclusion_set'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['personal.Personal']"}),
            'personal_incluido': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'inclusion_set'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['personal.Personal']"}),
            'proyecto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['proyectos.Proyecto']"})
        },
        'proyectos.modelocontrato': {
            'Meta': {'ordering': "['titulo']", 'object_name': 'ModeloContrato'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'proyectos.proyecto': {
            'Meta': {'ordering': "['-fecha_inicio']", 'object_name': 'Proyecto'},
            'aseguradoras': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['aseguradoras.Aseguradora']", 'symmetrical': 'False'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['clientes.Cliente']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'encargados': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['aseguradoras.Encargado']", 'symmetrical': 'False'}),
            'fecha_fin': ('django.db.models.fields.DateField', [], {}),
            'fecha_inicio': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.FloatField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'personal': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['personal.Personal']", 'symmetrical': 'False'})
        }
    }

    complete_apps = ['proyectos']