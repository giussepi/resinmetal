# -*- coding: utf-8 -*-
""" Proyectos Admin """

from django.conf import settings
from django.conf.urls import patterns, url
from django.contrib import admin
from django.contrib.formtools.wizard.views import SessionWizardView
from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.template import RequestContext
from correos import settings as settings_correos
from common.utils import return_pdf
from aseguradoras.models import Encargado
from proyectos.models import ModeloContrato, Proyecto, InclusionExclusion, \
    Contrato
from proyectos.forms import AdminModeloContratoForm, AdminContratoForm, \
    AdminInclusionExclusionForm0, AdminInclusionExclusionForm1, \
    AdminInclusionExclusionForm2, AdminInclusionExclusionForm, \
    AdminProyectoForm
from proyectos import settings as proyectos_settings
import os


class ModeloContratoAdmin(admin.ModelAdmin):
    """ admin model that manages the contracts """
    list_filter = ('tipo',)
    search_fields = ('^titulo',)
    save_as = True

    form = AdminModeloContratoForm


class ProyectoAdmin(admin.ModelAdmin):
    """ hay q aher un custom change view para que muestre los campos de
    personal, encargados y aseguradoras... """
    list_display = ('nombre', 'fecha_inicio', 'fecha_fin', 'cliente')
    list_display_links = ('nombre', 'fecha_inicio', 'fecha_fin', 'cliente')
    form = AdminProyectoForm
    fieldsets = (
        ('Datos del proyecto', {
            'fields': ('nombre', 'descripcion', ('fecha_inicio', 'fecha_fin'),
                       ('monto', 'cliente'), 'aseguradoras', 'encargados')
        }),
        ('Enviar renovación por correo', {
            'classes': ('collapse', ),
            'fields': ('user_sender', 'documentos', 'correos', 'asunto',
                       'texto'),
            'description': u'Si llena este formulario, los documentos\
            seleccionados de la renovación de contratos se enviarán por \
            correo. Si desea enviar por correo las inclusiones o exclusiones\
            utilice la página de las inclusiones exclusiones.'
        })
    )
    exclude = ('personal', )
    list_filter = ('aseguradoras', )
    search_fields = ('nombre', )
    date_hierarchy = 'fecha_inicio'
    add_form_template = 'admin/proyectos/proyecto/add_form.html'

    class Media:
        css = {
            'all': ('common/css/admin.css', )
        }

    def return_pdf_final_staff(self, request, project_id):
        """
        returns a PDF with the contracts of the workers that finished the
        project
        """
        project = get_object_or_404(Proyecto, id=project_id)
        contracts = ''
        for contract in project.contrato_set.exclude(
                tipo_inclusionexclusion=u'e'):
            if contracts:
                contracts += '<pdf:nextpage>'
            contracts += contract.texto
        if contracts:
            return return_pdf(
                contracts, u'proyecto_%d_equipo_final' % project.id)
        return False

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """
        add the initial staff, last staff and the inclusions exclusions as an
        extra_context.
        """
        extra_context = extra_context or {}
        proyecto = get_object_or_404(Proyecto, id=object_id)
        extra_context['initial_staff'] = proyecto.contrato_set.filter(
            equipo_inicial=True)
        extra_context['inclusions_exclusions'] = \
            proyecto.get_contracts_from_inclusions_exclusions()
        extra_context['final_staff'] = proyecto.contrato_set.exclude(
            tipo_inclusionexclusion=u'e')
        extra_context[
            'CONTRACTS_URL'] = proyectos_settings.CONTRATOS_PDF_FOLDER
        extra_context['ATTACHMENTS_URL'] = settings_correos.ATTACHMENTS_FOLDER
        return super(ProyectoAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context)

    def get_urls(self):
        urls = super(self.__class__, self).get_urls()
        my_urls = patterns(
            '',
            url(r'^return_final_staff/(?P<project_id>\d+)/$',
                self.admin_site.admin_view(self.return_pdf_final_staff),
                name='admin_proyectos_proyecto_return_pdf_final_staff'),
        )
        return my_urls + urls


class InclusionExclusionAdmin(admin.ModelAdmin):
    """ model admin that manages the inclusions exclusions """
    list_display = ('__unicode__', 'fecha_inicio')
    list_display_links = ('__unicode__', 'fecha_inicio')
    search_fields = ('proyecto__nombre', )
    date_hierarchy = 'fecha'
    # fields = ('proyecto', ('personal_incluido', 'personal_excluido'),
    #           'encargados', 'correos')
    fieldsets = (
        (None, {
            'fields': ('proyecto', ('personal_incluido', 'personal_excluido'),
                       'encargados')
        }),
        ('Enviar por correo', {
            'fields': ('user_sender', 'correos', 'asunto', 'texto'),
            'description': u'Si desea enviar los contratos del personal\
            incluido y el documento en excel de la inclusión exclusión, llene\
            los siguientes campos.'
        })
    )
    readonly_fields = ('proyecto', 'personal_incluido', 'personal_excluido',
                       'encargados')
    form = AdminInclusionExclusionForm

    class Media:
        css = {
            "all": ("common/css/admin.css", ),
        }

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """ adds the PDF download link if that document exists """
        extra_context = extra_context or {}
        download_url = None
        obj = get_object_or_404(InclusionExclusion, id=object_id)
        pdf_path = os.path.join(
            settings.MEDIA_ROOT, proyectos_settings.CONTRATOS_PDF_FOLDER,
            'contrato_proyecto_%d_inclusionexclusion_%d.pdf' % (
                obj.proyecto.id, obj.id))
        if os.path.exists(pdf_path):
            download_url = settings.MEDIA_URL +\
                proyectos_settings.CONTRATOS_PDF_FOLDER +\
                '/contrato_proyecto_%d_inclusionexclusion_%d.pdf' % (
                obj.proyecto.id, obj.id)
        download_url2 = settings.MEDIA_URL +\
            settings_correos.ATTACHMENTS_FOLDER +\
            '/proyecto_%d_inclusionexclusion_%d.xls' % (
                obj.proyecto.id, obj.id)
        extra_context['PDF_download_url'] = download_url
        extra_context['XLS_download_url'] = download_url2
        return super(InclusionExclusionAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context)

    def get_urls(self):
        urls = super(self.__class__, self).get_urls()
        my_urls = patterns(
            '',
            url(r'^add/$',
                self.admin_site.admin_view(
                    self.AdminInclusionExclusionWizard.as_view(
                        [AdminInclusionExclusionForm0,
                         AdminInclusionExclusionForm1,
                         AdminInclusionExclusionForm2],)),
                name='admin_proyectos_add_inclusionexclusion'),
            url(r'^add_successful/(?P<inclusionexclusion_id>\d+)/$',
                self.admin_site.admin_view(self.add_successful),
                name='admin_proyectos_add_inclusionexclusion_successful'),
        )
        return my_urls + urls

    class AdminInclusionExclusionWizard(SessionWizardView):
        """ inclusions exclusions wizard """
        template_name = 'admin/proyectos/inclusionexclusion/add_form.html'

        def get_form_initial(self, step):
            """
            uses the data from previous forms to initialize the next forms
            """
            if step == '1':
                project = Proyecto.objects.get(id=self.request.session[
                    'wizard_admin_inclusion_exclusion_wizard']['step_data'][
                        '0']['0-proyecto'][0])
                id_modelocontrato = self.request.session[
                    'wizard_admin_inclusion_exclusion_wizard'][
                        'step_data']['0']['0-modelo_contrato'][0]
                return {'proyecto': project.id,
                        'encargados': project.encargados.all(),
                        'modelo_contrato': ModeloContrato.objects.get(
                            id=id_modelocontrato).texto}
            if step == '2':
                encargados_id = self.request.session[
                    'wizard_admin_inclusion_exclusion_wizard']['step_data'][
                        '1']['1-encargados']
                return {'user_sender': self.request.user,
                        'correos': ','.join(Encargado.objects.filter(
                            id__in=encargados_id).values_list(
                                'email', flat=True)),
                        'proyecto': self.request.session[
                            'wizard_admin_inclusion_exclusion_wizard'][
                                'step_data']['0']['0-proyecto'][0]}
            return self.initial_dict.get(step, {})

        def done(self, form_list, **kwargs):
            """ saves the forms and redirects to the successful page """
            results = []
            for form in form_list:
                results.append(form.save())
            return redirect(
                'admin:admin_proyectos_add_inclusionexclusion_successful',
                results[1].id
            )

    def add_successful(self, request, inclusionexclusion_id):
        """
        shows the page with the download link of the PDF with all the
        new contracts, it also shows the buttons to go back to the changelist
        page
        """
        obj = get_object_or_404(InclusionExclusion, id=inclusionexclusion_id)
        if request.method == 'GET' and 'go_back' in request.GET:
            return redirect('admin:proyectos_inclusionexclusion_changelist')
        pdf_path = os.path.join(
            settings.MEDIA_ROOT, proyectos_settings.CONTRATOS_PDF_FOLDER,
            'contrato_proyecto_%d_inclusionexclusion_%d.pdf' % (
                obj.proyecto.id, obj.id))
        exist = False
        if os.path.exists(pdf_path):
            exist = True
        return render_to_response(
            'admin/proyectos/inclusionexclusion/inclusionexclusion_done.html',
            {'project': obj.proyecto, 'inclusionexclusion': obj,
             'CONTRACTS_URL': proyectos_settings.CONTRATOS_PDF_FOLDER,
             'exist': exist},
            context_instance=RequestContext(request))


class ContratoAdmin(admin.ModelAdmin):
    """ admin model that manages the contracts """

    list_display = ('personal', 'cliente', 'tipo', 'proyecto',
                    'fecha_inicio', 'fecha_fin', )
    list_display_links = ('personal', 'cliente', 'tipo', 'proyecto',
                          'fecha_inicio', 'fecha_fin',)
    list_filter = ('tipo', 'proyecto__nombre',)
    search_fields = ('personal__user__last_name', 'cliente__user__last_name')
    date_hierarchy = 'fecha_inicio'
    fieldsets = (
        (u'', {
            'fields': (('fecha_inicio', 'fecha_fin', 'monto'), 'texto',
                       ('tipo', 'proyecto'))
        }),
        (u'Datos Extra', {
            'classes': ('collapse', ),
            'fields': ('contrato_escaneado',
                       'tipo_inclusionexclusion',
                       'cliente', 'personal',
                       'polizas', 'inclusionesexclusiones'),
            'description': u'Cuando tenga el contrato firmado escanéelo y \
            súbalo.',
        }),
    )
    form = AdminContratoForm
    readonly_fields = ('fecha_inicio', 'fecha_fin', 'monto',
                       'tipo', 'proyecto',
                       'tipo_inclusionexclusion', 'cliente', 'personal',
                       'inclusionesexclusiones', 'polizas')

    def get_actions(self, request):
        """ disables actions for this ModelAdmin """
        actions = super(self.__class__, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_add_permission(self, request):
        """ disables the creation of new contracts by the admin"""
        return False

    def has_delete_permission(self, request, obj=None):
        """ disables the deletion of an specified contract by the admin """
        return False

    def download_contract(self, request, contract_id):
        """ if the id contract exists, generates and returns contract PDF """
        contrato = get_object_or_404(Contrato, id=contract_id)
        return return_pdf(
            contrato.texto, u'%s' % contrato.personal)

    def get_urls(self):
        urls = super(self.__class__, self).get_urls()
        my_urls = patterns(
            '',
            url(r'^download_contract/(?P<contract_id>\d+)/$',
                self.admin_site.admin_view(self.download_contract),
                name='admin_proyectos_contrato_download'),
        )
        return my_urls + urls


admin.site.register(ModeloContrato, ModeloContratoAdmin)
admin.site.register(Proyecto, ProyectoAdmin)
admin.site.register(InclusionExclusion, InclusionExclusionAdmin)
admin.site.register(Contrato, ContratoAdmin)
