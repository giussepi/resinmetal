# -*- coding: utf-8 -*-
""" Proyectos tasks """

from django.core.exceptions import ObjectDoesNotExist
from celery import task
from proyectos.models import Proyecto


@task()
def finish_contract(project_id):
    """ reactivates the project staff """

    try:
        project = Proyecto.objects.get(id=project_id)
    except ObjectDoesNotExist:
        return 'ERROR: El proyecto no existe!'
    else:
        project.reactivate_staff()
        return 'Contratos terminados - Personal reactivado'
