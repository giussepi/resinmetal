# -*- coding: utf-8 -*-

""" Proyectos settings """

from django.conf import settings
from os import path

#GETTING VALUES DEFINED IN PROJECT SETTINGS
CONTRATOS_ESCANEADOS_FOLDER = path.join(
    'proyectos',
    getattr(settings, 'CONTRATOS_ESCANEADOS_FOLDER_NAME',
            'contratos_escaneados'), )

CONTRATOS_PDF_FOLDER = path.join(
    'proyectos',
    getattr(settings, 'CONTRATOS_PDF_FOLDER_NAME',
            'contratos_pdfs'), )
