# -*- coding: utf-8 -*-
""" proyectos forms """

from django import forms
from django.db import transaction
from django.conf import settings
from django.contrib.admin import widgets
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404, redirect
# from dateutil.relativedelta import relativedelta
from datetime import datetime, time, timedelta, date
from tinymce.widgets import TinyMCE
from xlrd import open_workbook
from xlwt import easyxf
from xlutils.copy import copy
from personal.models import Personal
from aseguradoras.models import Poliza, Aseguradora, Encargado
from common.utils import create_pdf, zip_files
from common.validators import validate_multiple_emails
from correos.models import Correo
from correos import settings as correos_settings
from proyectos.models import ModeloContrato, Contrato, Proyecto, \
    InclusionExclusion
from proyectos.tasks import finish_contract
from proyectos import settings as proyectos_settings
import re
import os


class AdminModeloContratoForm(forms.ModelForm):
    """ contract model form for the admin interface """

    class Meta:
        model = ModeloContrato

    def __init__(self, *args, **kwargs):
        super(AdminModeloContratoForm, self).__init__(*args, **kwargs)
        self.fields['texto'].widget = TinyMCE(attrs={'cols': 80, 'rows': 30})
        self.fields['titulo'].widget.attrs = {'size': 50}

    def clean_texto(self):
        """
        verifies that all the predefined patterns where used in the text, then
        verifies that all the patterns in the text match with one of the
        predefined patterns
        """
        text_patterns = re.findall('&lt;&lt;[a-z_]*&gt;&gt;',
                                   self.cleaned_data['texto'])
        for pattern in ModeloContrato.PATTERNS:
            if not text_patterns.__contains__(pattern):
                raise forms.ValidationError(
                    u'Debe incluir todos los patrones en el documento.')

        for pattern in text_patterns:
            if not ModeloContrato.PATTERNS.__contains__(pattern):
                raise forms.ValidationError(
                    u'Debe usar sólo los patrones indicados. Remueva o \
                    corrija el siguiente patrón <<%s>>' % pattern[8:-8])
        return self.cleaned_data['texto']


class AdminEditModeloContratoForm(AdminModeloContratoForm):
    """ edition contract model form for the admin interface """

    fecha_inicio = forms.DateField(widget=forms.HiddenInput)
    fecha_fin = forms.DateField(widget=forms.HiddenInput)
    monto = forms.DecimalField(widget=forms.HiddenInput)
    #num_meses = forms.IntegerField(min_value=1, widget=forms.HiddenInput)
    modelo_contrato = forms.ModelChoiceField(
        queryset=ModeloContrato.objects.all(), widget=forms.HiddenInput)
    personal_list = forms.CharField(widget=forms.HiddenInput)
    proyecto = forms.ModelChoiceField(queryset=Proyecto.objects.all(),
                                      widget=forms.HiddenInput)
    # cliente = forms.ModelChoiceField(queryset=Cliente.objects.all(),
    #                                  widget=forms.HiddenInput)
    poliza = forms.CharField(widget=forms.HiddenInput)
    modified = forms.IntegerField(initial=0, widget=forms.HiddenInput)
    update = forms.IntegerField(initial=0, widget=forms.HiddenInput)
    save_as = forms.IntegerField(initial=0, widget=forms.HiddenInput)

    def first_init(self, request):
        """ initializes the form with the data stored in a session variable """
        data = request.session.get('select_modelo_contrato')
        modelo = get_object_or_404(ModeloContrato,
                                   id=data['modelo_contrato'])
        return AdminEditModeloContratoForm(initial={
            'fecha_inicio': data['fecha_inicio'],
            'fecha_fin': data['fecha_fin'],
            'monto': data['monto'],
            'modelo_contrato': modelo.id,
            'personal_list': data['personal_list'],
            'proyecto': data['proyecto'],
            # 'cliente': data['cliente'],
            'poliza': data['poliza'],
            'titulo': modelo.titulo,
            'texto': modelo.texto,
            'tipo': modelo.tipo,
        })

    @transaction.commit_on_success
    def save(self, request, *args, **kwargs):
        """
        updates or saves de ModeloContrato object if it's necessary.
        Sets the selected personal to the hired state.
        # Creates the relation between the selected Project and Client.
        Adds the selected Personal objects to the m2m relationship between
        Proyecto and Personal.
        Creates and saves all the contracts for each personal selected.
        Launches the asynchronous task to reactivate the staff proyect one day
        after the project end date.
        Relates the project with the insurance companies of the selected
        polizas.
        Creates the project PDF with all the contracts.
        Deletes from the session all the data used during the contract process.
        Returns a redirect to the contract_done view.

        # SOLO CREA CORRECTAMENTE LOS CONTRATOS, NO ESTA RELACIONANDO EL
        # PROYECTO CON LA ASEGURADORA Y LA POLIZA, X LO TANTO LoS SEGUROS D LOS
        # CONTRATOS NO COINCIDEN CON LOS SEGUROS ELEGIDOS AL CREAR AL PROYECTO
        # ver si se deja los encargados y aseguradoas en el form de crear
        # proyecto o se deja aqui
        # SOLUCION
        # hare q al crear un proyecto, las aseguradoras y encargados no se
        # elijan aun, las aseguradoras se agregaran al elegir la poliza y los
        # encargados se agregaran al proyecto segun se seleccionen al
        # asegurar el personal y al hacer inclusiones exclusiones ()
        """
        if self.cleaned_data['update'] == 1:
            modelo = self.cleaned_data['modelo_contrato']
            modelo.titulo = self.cleaned_data['titulo']
            modelo.texto = self.cleaned_data['texto']
            modelo.tipo = self.cleaned_data['tipo']
            modelo.save()
        elif self.cleaned_data['save_as'] == 1:
            modelo = ModeloContrato.objects.create(
                titulo=self.cleaned_data['titulo'],
                texto=self.cleaned_data['texto'],
                tipo=self.cleaned_data['tipo'])

        personal_queryset = Personal.objects.filter(
            id__in=eval(self.cleaned_data['personal_list']))
        personal_queryset.update(estado=u'contratado')
        try:
            current_num_contracts = Contrato.objects.latest('id')
        except ObjectDoesNotExist:
            current_num_contracts = 0
        else:
            current_num_contracts = current_num_contracts.id
        contracts_list = []
        text_replaced_patterns = u''
        all_contracts = u''
        data = []
        iteration = 0
        # aca si mantengo al cliente seleccionado al crear el proyecto
        # el cliente seria obtenido directamente del proyecto.
        # eso siempre y cuando el cliente sea obligatorio
        # por ahora ser'a asi
        proyecto = self.cleaned_data['proyecto']
        # proyecto.cliente = self.cleaned_data['cliente']
        # proyecto.save()
        for personal in personal_queryset:
            proyecto.personal.add(personal)
            text_replaced_patterns = self.cleaned_data['texto']
            data = [personal.__unicode__(), personal.dni,
                    personal.direcciones.all()[0].full_address(),
                    self.cleaned_data['fecha_inicio'].strftime('%d/%m/%y'),
                    self.cleaned_data['fecha_fin'].strftime('%d/%m/%y')]
            iteration = 0
            for pattern in ModeloContrato.PATTERNS:
                text_replaced_patterns = text_replaced_patterns.replace(
                    pattern, data[iteration])
                iteration = iteration + 1
            current_num_contracts = current_num_contracts + 1
            if all_contracts:
                all_contracts += '<pdf:nextpage>'
            all_contracts += text_replaced_patterns
            contracts_list.append(
                Contrato(id=current_num_contracts,
                         fecha_inicio=self.cleaned_data['fecha_inicio'],
                         fecha_fin=self.cleaned_data['fecha_fin'],
                         monto=self.cleaned_data['monto'],
                         texto=text_replaced_patterns,
                         tipo=self.cleaned_data['tipo'],
                         equipo_inicial=True,
                         proyecto=proyecto,
                         #cliente=self.cleaned_data['cliente'],
                         cliente=proyecto.cliente,
                         # polizas=self.cleaned_data['poliza'],
                         personal=personal,
                         )
            )
        Contrato.objects.bulk_create(contracts_list)
        finish_contract.apply_async((proyecto.id,), eta=datetime.combine(
            self.cleaned_data['fecha_fin'] + timedelta(1), time()))
        # ddd = datetime.now() + timedelta(minutes=1)
        # finish_contract.apply_async(
        #     (proyecto.id,),
        #     eta=ddd)
        poliza_list = eval(self.cleaned_data['poliza'])
        for contrato in contracts_list:
            contrato.polizas = poliza_list
        for poliza_id in poliza_list:
            proyecto.aseguradoras.add(
                Poliza.objects.get(id=poliza_id).aseguradora)
        create_pdf(
            'contrato_proyecto_%d.pdf' % proyecto.id,
            all_contracts,
            os.path.join(settings.MEDIA_ROOT,
                         proyectos_settings.CONTRATOS_PDF_FOLDER))
        del request.session['select_modelo_contrato']
        if 'especialidades' in request.session:
            del request.session['especialidades']
        if 'min_experience' in request.session:
            del request.session['min_experience']
        return redirect('admin:admin_personal_contrato_done', proyecto.id)

    def model_has_changed(self, request):
        """
        returns True if some part of the model has been modified, otherwise
        returns False.
        ToDo: when hiring process was finished and the model was changed too,
        if in the view contract_donde when a go back action is done the
        redirection to the changelist does not works, instead of that the popin
        is shown again and after pushing a buttom an error happens here, this
        avoids the double hiring but it is not an appropriate method to do it.
        It's necessary to find a solution for this bug.
        """
        data = request.session.get('select_modelo_contrato')
        modelo = get_object_or_404(ModeloContrato,
                                   id=data['modelo_contrato'])
        if self.cleaned_data['modified'] == 1:
            return False
        if self.cleaned_data['titulo'] != modelo.titulo or (
                self.cleaned_data['texto'] != modelo.texto or
                self.cleaned_data['tipo'] != modelo.tipo):
            cd = self.data.copy()
            cd['modified'] = 1
            self.data = cd
            return True
        return False


class AdminSelectModeloContratoForm(forms.Form):
    """ selection contract model form for the admin interface """

    fecha_inicio = forms.DateField(widget=widgets.AdminDateWidget())
    #num_meses = forms.IntegerField(min_value=1)
    fecha_fin = forms.DateField(widget=widgets.AdminDateWidget())
    monto = forms.DecimalField(help_text=u'remuneración de los trabajadores.')
    modelo_contrato = forms.ModelChoiceField(
        queryset=ModeloContrato.objects.all())
    personal_list = forms.CharField(widget=forms.HiddenInput)
    proyecto = forms.ModelChoiceField(
        queryset=Proyecto.objects.filter(personal__isnull=True))
    #cliente = forms.ModelChoiceField(queryset=Cliente.objects.all())
    poliza = forms.ModelMultipleChoiceField(
        queryset=Poliza.objects.filter(vigente=True))

    def clean(self):
        """
        verifies that the contract start date is earlier than the contract
        end date.
        verifies that the contract end date is not later than the project
        ends date.
        """
        cd = super(self.__class__, self).clean()
        if 'fecha_inicio' in cd and 'fecha_fin' in cd:
            if cd['fecha_inicio'] >= cd['fecha_fin']:
                raise forms.ValidationError(u'La fecha de inicio no puede ser \
            igual o posterior a la fecha de finalización.')
        if 'fecha_fin' in cd and 'proyecto' in cd:
            if cd['fecha_fin'] > cd['proyecto'].fecha_fin:
                raise forms.ValidationError(u'El contrato no puede terminar \
            después de la fecha de finalización del proyecto seleccionado \
            (%s).' % cd['proyecto'].fecha_fin.strftime('%d/%m/%y'))
        return cd

    def first_init(self, request):
        """
        if the request has POST data returns a form  with the personal_list
        initialized with the integer values in the POST data, else
        returns a form initialized with the data in the
        'select_modelo_contrato' session variable and removes this variable
        from de session variables
        """
        if request.POST:
            lista = []
            val = 0
            for value in request.POST.values():
                try:
                    val = int(value)
                except ValueError:
                    pass
                else:
                    lista.append(val)
            return self.__class__(initial={'personal_list': lista})
        else:
            return self.__class__(
                initial=request.session.pop('select_modelo_contrato'))

    def save(self, request):
        """ copies the form cleaned data to a session variable """
        cd = self.cleaned_data
        request.session['select_modelo_contrato'] = {
            'fecha_inicio': cd['fecha_inicio'].strftime('%d/%m/%y'),
            'fecha_fin': cd['fecha_fin'].strftime('%d/%m/%y'),
            'monto': cd['monto'],
            'modelo_contrato': cd['modelo_contrato'].id,
            'personal_list': cd['personal_list'],
            'proyecto': cd['proyecto'].id,
            # 'cliente': cd['cliente'].id,
            'poliza': [pol.id for pol in cd['poliza']],
        }


class AdminContratoForm(forms.ModelForm):
    """ contract form for the admin interface """

    class Meta:
        model = Contrato

    def __init__(self, *args, **kwargs):
        super(AdminContratoForm, self).__init__(*args, **kwargs)
        self.fields['texto'].widget = TinyMCE(
            attrs={'cols': 80, 'rows': 30}, mce_attrs={'readonly': True})


class AdminAssureForm(forms.ModelForm):
    """ assure form for the admin interface """

    fecha_inicio_contrato = forms.DateField(widget=forms.HiddenInput)
    fecha_fin_contrato = forms.DateField(widget=forms.HiddenInput)
    polizas = forms.CharField(max_length=250, widget=forms.HiddenInput)
    fecha_inicio_seguro = forms.DateField(widget=widgets.AdminDateWidget())
    fecha_fin_seguro = forms.DateField(widget=widgets.AdminDateWidget())
    encargados = forms.ModelMultipleChoiceField(
        label=u'Enviar a',
        queryset=Encargado.objects.filter(activo=True),
        widget=widgets.FilteredSelectMultiple(
            'Encargados', is_stacked=False))
    extra_email = forms.CharField(
        label=u'Copiar a', required=False,
        widget=forms.TextInput(attrs={'size': 80}),
        validators=[validate_multiple_emails],
        help_text=u'Los correos deben estar separados por una coma.')

    class Meta:
        model = Correo
        exclude = ('inclusionexclusion')

    def __init__(self, *args, **kwargs):
        super(AdminAssureForm, self).__init__(*args, **kwargs)
        self.fields['correos'].widget = forms.HiddenInput()
        self.fields['user_sender'].widget = forms.HiddenInput()
        self.fields['proyecto'].widget = forms.HiddenInput()
        self.fields['contrato'].widget = forms.HiddenInput()
        self.fields['asunto'].widget = forms.TextInput(attrs={'size': 80})
        self.fields['texto'].widget = forms.Textarea(
            attrs={'cols': 80, 'rows': 15})
        self.fields['archivo_adjunto'].widget = forms.HiddenInput()
        self.fields.keyOrder = [
            'fecha_inicio_contrato', 'fecha_fin_contrato', 'polizas',
            'fecha_inicio_seguro', 'fecha_fin_seguro', 'encargados',
            'extra_email',
            'correos', 'asunto', 'texto', 'archivo_adjunto',
            'user_sender', 'proyecto', 'contrato']

    def first_init(self, project_id, user):
        """
        initializes the form fields with data gathered using the project id
        """
        project = get_object_or_404(Proyecto, id=project_id)
        contract = project.contrato_set.filter(
            inclusionesexclusiones__isnull=True)[0]
        aseguradoras_list = contract.polizas.values_list(
            'aseguradora__id', flat=True)
        encargados_list = Encargado.objects.filter(
            aseguradora__id__in=aseguradoras_list)
        return self.__class__(initial={
            'fecha_inicio_contrato': contract.fecha_inicio.strftime(
                '%d/%m/%y'),
            'fecha_fin_contrato': contract.fecha_fin.strftime('%d/%m/%y'),
            'encargados': encargados_list,
            'polizas': contract.polizas.values_list('id', flat=True),
            'user_sender': user,
            'correos': ', '.join(encargados_list.values_list('email',
                                                             flat=True)),
            'proyecto': project,
        })

    def clean(self):
        """
        verifies that the insurance start date is earlier than the
        insurance end date.
        Copies all the destination mails to the correos field.
        """
        cd = super(self.__class__, self).clean().copy()
        if 'fecha_inicio_seguro' in cd and 'fecha_fin_seguro' in cd:
            if cd['fecha_inicio_seguro'] >= cd['fecha_fin_seguro']:
                raise forms.ValidationError(u'La fecha de inicio no puede ser \
                igual o posterior a la fecha de finalización.')
        if cd.get('extra_email'):
            cd['correos'] = cd['extra_email'] + ',' + ','.join(
                cd['encargados'].values_list('email', flat=True))
        else:
            cd['correos'] = ','.join(
                cd['encargados'].values_list('email', flat=True))
        return cd

    @transaction.commit_on_success
    def save(self, request, *args, **kwargs):
        """
        Updates the start and end dates of the Poliza objects selected.
        If the path to the attached files does not exist, it's created.
        Creates an excel document filling the corresponding data in the
        sheet number 4 of the file template.xls.
        Relates the project with the selected personal of the insurance
        companies(encargados).
        Creates and saves (also sends) the email object attaching the xls file.
        """
        cd = self.cleaned_data
        #email_saved = super(AdminAssureForm, self).save(*args, **kwargs)
        Poliza.objects.filter(id__in=eval(cd['polizas'])).update(
            vigente=True, fecha_inicio_vigencia=cd['fecha_inicio_seguro'],
            fecha_fin_vigencia=cd['fecha_fin_seguro'])
        # debe haber un proceso q diariamente verifique si las polizas
        # aun son validas
        rb = open_workbook('personal/excel_template/template.xls',
                           formatting_info=True)
        wb = copy(rb)
        ws = wb.get_sheet(4)
        row = 1
        persona_counter = 0
        tmp_lastname = u''
        date_format = easyxf(num_format_str='DD-MM-YYYY')
        monto = cd['proyecto'].contrato_set.latest('id').monto
        for persona in cd['proyecto'].personal.iterator():
            persona_counter += 1
            ws.row(row).set_cell_number(0, persona_counter)
            ws.row(row).set_cell_text(1, persona.user.first_name)
            tmp_lastname = persona.user.last_name.split()
            ws.row(row).set_cell_text(2, tmp_lastname[0])
            ws.row(row).set_cell_text(3, tmp_lastname[1])
            ws.row(row).set_cell_text(6, persona.dni)
            ws.row(row).set_cell_text(7, persona.get_sexo_display())
            ws.row(row).set_cell_text(8, persona.get_estado_civil_display())
            ws.row(row).set_cell_text(
                9, persona.direcciones.all()[0].full_address())
            ws.row(row).set_cell_text(
                10, u'/'.join(persona.telefonos.values_list(
                    'numero', flat=True)))
            if persona.fecha_nacimiento:
                ws.row(row).set_cell_date(11, persona.fecha_nacimiento,
                                          date_format)
            ws.row(row).set_cell_text(12, persona.user.email)
            ws.row(row).set_cell_text(13, '(PEN) Nuevo Sol Peruano')
            ws.row(row).set_cell_number(14, monto)
            row += 1
        path = os.path.join(
            settings.MEDIA_ROOT, correos_settings.ATTACHMENTS_FOLDER)
        if not os.path.exists(path):
            os.makedirs(path)
        wb.save(os.path.join(path, 'proyecto_%d.xls' % cd['proyecto'].id))
        project = cd['proyecto']
        for encargado in cd['encargados']:
            project.encargados.add(encargado)
        Correo.objects.create(
            correos=cd['correos'],
            asunto=cd['asunto'],
            texto=cd['texto'],
            archivo_adjunto=os.path.join(
                correos_settings.ATTACHMENTS_FOLDER,
                'proyecto_%d.xls' % cd['proyecto'].id),
            user_sender=cd['user_sender'],
            proyecto=cd['proyecto'])
        messages.success(
            request, u'Los correos para asegurar al personal fueron enviados \
            con éxito.')
        return redirect('admin:personal_personal_changelist')


class AdminInclusionExclusionForm0(forms.Form):
    """ first inclusion excluron form for the admin interface """

    proyecto = forms.ModelChoiceField(
        queryset=Proyecto.objects.filter(personal__isnull=False).distinct())
    modelo_contrato = forms.ModelChoiceField(
        queryset=ModeloContrato.objects.all())

    def clean_proyecto(self):
        """
        verifies that the end date of the project contracts is greater or equal
        than the actual date.
        """
        contract = self.cleaned_data['proyecto'].contrato_set.latest('id')
        if contract.fecha_fin < date.today():
            raise forms.ValidationError(u'Los contratos relacionados al \
        proyecto seleccionados ya han caducado el %s. Sólo puede hacer \
        inclusiones exclusiones en proyectos cuyos contratos no hayan \
        terminado.' % contract.fecha_fin.strftime('%d/%m/%y'))
        return self.cleaned_data['proyecto']

    def save(self):
        """ just returns True """
        return True


class AdminInclusionExclusionForm1(forms.ModelForm):
    """ second inclusion excluron form for the admin interface """

    modelo_contrato = forms.CharField(
        help_text=u'Los datos o patrones que cambiarán de contrato en \
        contrato son: %s' % ', '.join(ModeloContrato.PATTERNS.__iter__()),
        widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))
    # monto = forms.DecimalField()

    class Meta:
        model = InclusionExclusion

    def __init__(self, *args, **kwargs):
        """
        """
        super(AdminInclusionExclusionForm1, self).__init__(*args, **kwargs)
        self.fields['fecha_inicio'].widget = widgets.AdminDateWidget()
        self.fields[
            'personal_incluido'].widget = widgets.FilteredSelectMultiple(
                'Personas', is_stacked=False)
        self.fields['personal_incluido'].queryset = Personal.objects.filter(
            estado=u'viable')
        self.fields[
            'personal_excluido'].widget = widgets.FilteredSelectMultiple(
                'Personas', is_stacked=False)
        self.fields['personal_incluido'].label = u'Personal a incluir'
        self.fields['personal_excluido'].queryset = Personal.objects.filter(
            id__in=Proyecto.objects.get(
                id=self.initial['proyecto']).contrato_set.exclude(
                    tipo_inclusionexclusion=u'e').values_list(
                        'personal', flat=True))
        self.fields['personal_excluido'].label = u'Personal a excluir'
        self.fields['encargados'].widget = widgets.FilteredSelectMultiple(
            'Encargados', is_stacked=False)
        self.fields['encargados'].queryset = Encargado.objects.filter(
            activo=True)
        self.fields['proyecto'].widget = forms.HiddenInput()
        self.fields.keyOrder = (
            'proyecto', 'personal_excluido', 'personal_incluido',
            'fecha_inicio', 'modelo_contrato', 'encargados',
        )

    # def clean_monto(self):
    #     """ verifies that the salary > 0 """
    #     if self.cleaned_data['monto'] <= 0:
    #         raise forms.ValidationError(u'El monto debe ser mayor que 0.')
    #     return self.cleaned_data['monto']

    def clean_fecha_inicio(self):
        """
        verifies that the start date is between the contract start and
        end dates
        """
        contract = self.cleaned_data['proyecto'].contrato_set.order_by('id')[0]
        if self.cleaned_data['fecha_inicio'] < contract.fecha_inicio:
            raise forms.ValidationError(u'La fecha de inicio de la \
inclusión no puede ser menor a la fecha de inicio de los contratos (%s) \
relacionados a este proyecto.' % contract.fecha_inicio.strftime('%d/%m/%y'))
        if self.cleaned_data['fecha_inicio'] >= contract.fecha_fin:
            raise forms.ValidationError(u'La fecha de inicio de la \
inclusión no puede ser mayor o igual a la fecha de finalización de los \
contratos (%s) relacionados a este \
proyecto.' % contract.fecha_fin.strftime('%d/%m/%y'))
        return self.cleaned_data['fecha_inicio']

    def clean_modelo_contrato(self):
        """
        verifies that all the predefined patterns where used in the text, then
        verifies that all the patterns in the text match with one of the
        predefined patterns
        """
        text_patterns = re.findall('&lt;&lt;[a-z_]*&gt;&gt;',
                                   self.cleaned_data['modelo_contrato'])
        for pattern in ModeloContrato.PATTERNS:
            if not text_patterns.__contains__(pattern):
                raise forms.ValidationError(
                    u'Debe incluir todos los patrones en el documento.')

        for pattern in text_patterns:
            if not ModeloContrato.PATTERNS.__contains__(pattern):
                raise forms.ValidationError(
                    u'Debe usar sólo los patrones indicados. Remueva o \
                    corrija el siguiente patrón <<%s>>' % pattern[8:-8])
        return self.cleaned_data['modelo_contrato']

    def clean(self):
        """
        verifies that personal objects were selected for including and/or
        excluding.
        """
        cd = super(AdminInclusionExclusionForm1, self).clean()
        if not cd.get('personal_excluido') and not cd.get('personal_incluido'):
            raise forms.ValidationError(u'Debe seleccionar personal a incluir \
y/o excluir.')
        return cd

    @transaction.commit_on_success
    def save(self, *args, **kwargs):
        """
        Creates de inclusionexclusion.
        Creates a new excel document for the inclusion and/or exclusion.
        If there are persons to be excluded:
        * For each contract of the personal to be excluded:
          - Sets the excluded type.
          - Adds the relation to the inclusionexclusion.
        * Sets the state 'viable' to all the personal to be excluded.
        * Fills the exclusion sheet of the excel document with the data of
          the personal to be excluded.
        If there are persons to be included:
        * Sets the state 'contratado' to all the personal to be included.
        * For each personal to be included:
          - Relates the project with the personal.
          - Creates his contract.
          - Writes his data in the inclusion sheet of the excel document.
        * For each inclusion contract:
          - Relates the insurances used with the project original contracts
            with each one of the contracts.
          - Relates the contract with the inclusionexclusion.
        * Creates the PDF document with the contracts.
        Relates the project with the selected personal of the insurance
        companies.
        Returns the inclusionexclusion just created.
        """
        inclusionexclusion = super(AdminInclusionExclusionForm1, self).save()
        proyecto = self.cleaned_data['proyecto']
        old_contract = proyecto.contrato_set.order_by('id')[0]
        rb = open_workbook('personal/excel_template/template.xls',
                           formatting_info=True)
        wb = copy(rb)
        ws = wb.get_sheet(3)
        row = 1
        tmp_lastname = u''
        date_format = easyxf(num_format_str='DD-MM-YYYY')
        if self.cleaned_data.get('personal_excluido'):
            for contrato in self.cleaned_data['proyecto'].contrato_set.\
                exclude(tipo_inclusionexclusion=u'e').\
                filter(personal__in=self.cleaned_data.get(
                    'personal_excluido').values_list('id', flat=True)):
                contrato.tipo_inclusionexclusion = u'e'
                contrato.inclusionesexclusiones.add(inclusionexclusion)
                contrato.save()
            Personal.objects.filter(id__in=[
                per.id for per in self.cleaned_data[
                    'personal_excluido']]).update(estado=u'viable')
            for persona in self.cleaned_data['personal_excluido']:
                ws.row(row).set_cell_text(0, persona.user.first_name)
                tmp_lastname = persona.user.last_name.split()
                ws.row(row).set_cell_text(1, tmp_lastname[0])
                ws.row(row).set_cell_text(2, tmp_lastname[1])
                ws.row(row).set_cell_text(4, 'DNI')
                ws.row(row).set_cell_text(5, persona.dni)
                ws.row(row).set_cell_text(6, persona.get_sexo_display())
                ws.row(row).set_cell_text(
                    7, persona.get_estado_civil_display())
                ws.row(row).set_cell_text(
                    8, persona.direcciones.all()[0].full_address())
                ws.row(row).set_cell_text(
                    9, u'/'.join(persona.telefonos.values_list(
                        'numero', flat=True)))
                if persona.fecha_nacimiento:
                    ws.row(row).set_cell_date(
                        10, persona.fecha_nacimiento, date_format)
                ws.row(row).set_cell_text(11, persona.user.email)
                ws.row(row).set_cell_text(12, '(PEN) Nuevo Sol Peruano')
                ws.row(row).set_cell_number(13, old_contract.monto)
                ws.row(row).set_cell_date(
                    14, self.cleaned_data['fecha_inicio'], date_format)
                row += 1

        ws = wb.get_sheet(2)
        row = 1
        if self.cleaned_data.get('personal_incluido'):
            personal_queryset = Personal.objects.filter(
                id__in=[per.id for per in self.cleaned_data[
                    'personal_incluido']])
            personal_queryset.update(estado=u'contratado')
            try:
                current_num_contracts = Contrato.objects.latest('id')
            except ObjectDoesNotExist:
                current_num_contracts = 0
            else:
                current_num_contracts = current_num_contracts.id
            contracts_list = []
            text_replaced_patterns = u''
            all_contracts = u''
            data = []
            iteration = 0
            for personal in personal_queryset:
                proyecto.personal.add(personal)
                text_replaced_patterns = self.cleaned_data['modelo_contrato']
                data = [personal.__unicode__(), personal.dni,
                        personal.direcciones.all()[0].full_address(),
                        self.cleaned_data['fecha_inicio'].strftime('%d/%m/%y'),
                        old_contract.fecha_fin.strftime('%d/%m/%y')]
                iteration = 0
                for pattern in ModeloContrato.PATTERNS:
                    text_replaced_patterns = text_replaced_patterns.replace(
                        pattern, data[iteration])
                    iteration = iteration + 1
                current_num_contracts = current_num_contracts + 1
                if all_contracts:
                    all_contracts += '<pdf:nextpage>'
                all_contracts += text_replaced_patterns
                contracts_list.append(
                    Contrato(id=current_num_contracts,
                             fecha_inicio=self.cleaned_data['fecha_inicio'],
                             fecha_fin=old_contract.fecha_fin,
                             monto=old_contract.monto,
                             texto=text_replaced_patterns,
                             tipo=old_contract.tipo,
                             proyecto=proyecto,
                             cliente=old_contract.cliente,
                             personal=personal,
                             tipo_inclusionexclusion=u'i',
                             #inclusionesexclusiones=[inclusionexclusion.id]
                             )
                )
                ws.row(row).set_cell_text(0, personal.user.first_name)
                tmp_lastname = personal.user.last_name.split()
                ws.row(row).set_cell_text(1, tmp_lastname[0])
                ws.row(row).set_cell_text(2, tmp_lastname[1])
                ws.row(row).set_cell_text(4, 'DNI')
                ws.row(row).set_cell_text(5, personal.dni)
                ws.row(row).set_cell_text(6, personal.get_sexo_display())
                ws.row(row).set_cell_text(
                    7, personal.get_estado_civil_display())
                ws.row(row).set_cell_text(
                    8, personal.direcciones.all()[0].full_address())
                ws.row(row).set_cell_text(
                    9, u'/'.join(personal.telefonos.values_list(
                        'numero', flat=True)))
                if personal.fecha_nacimiento:
                    ws.row(row).set_cell_date(
                        10, personal.fecha_nacimiento, date_format)
                ws.row(row).set_cell_text(11, personal.user.email)
                ws.row(row).set_cell_text(12, '(PEN) Nuevo Sol Peruano')
                ws.row(row).set_cell_number(13, old_contract.monto)
                ws.row(row).set_cell_date(
                    14, self.cleaned_data['fecha_inicio'], date_format)
                row += 1
            Contrato.objects.bulk_create(contracts_list)
            for contrato in contracts_list:
                contrato.polizas = old_contract.polizas.values_list(
                    'id', flat=True)
                contrato.inclusionesexclusiones.add(inclusionexclusion)
            create_pdf(
                'contrato_proyecto_%d_inclusionexclusion_%d.pdf' % (
                    proyecto.id, inclusionexclusion.id),
                all_contracts,
                os.path.join(settings.MEDIA_ROOT,
                             proyectos_settings.CONTRATOS_PDF_FOLDER))
        wb.save(
            os.path.join(
                settings.MEDIA_ROOT,
                correos_settings.ATTACHMENTS_FOLDER,
                'proyecto_%d_inclusionexclusion_%d.xls' % (
                    proyecto.id, inclusionexclusion.id)
            )
        )
        for encargado in self.cleaned_data['encargados']:
            proyecto.encargados.add(encargado)
        return inclusionexclusion


class AdminInclusionExclusionForm2(forms.ModelForm):
    """ third inclusion excluron form for the admin interface """

    extra_email = forms.CharField(
        label=u'Copiar a', required=False,
        widget=forms.TextInput(attrs={'size': 80}),
        validators=[validate_multiple_emails],
        help_text=u'Los correos deben estar separados por una coma.')

    class Meta:
        model = Correo
        exclude = ('contrato', 'archivo_adjunto', )

    def __init__(self, *args, **kwargs):
        super(AdminInclusionExclusionForm2, self).__init__(*args, **kwargs)
        self.fields['asunto'].widget = forms.TextInput(attrs={'size': 80})
        self.fields['texto'].widget = forms.Textarea(
            attrs={'cols': 80, 'rows': 15})
        self.fields['correos'].widget = forms.HiddenInput()
        self.fields['user_sender'].widget = forms.HiddenInput()
        self.fields['proyecto'].widget = forms.HiddenInput()
        self.fields['inclusionexclusion'].widget = forms.HiddenInput()
        self.fields.keyOrder = (
            'extra_email', 'asunto', 'texto', 'correos', 'user_sender',
            'proyecto', 'inclusionexclusion'
        )

    def clean(self):
        """
        copies all the extra mails to the correos field.
        """
        cd = super(self.__class__, self).clean().copy()
        if cd.get('extra_email'):
            cd['correos'] = cd['extra_email'] + ',' + cd['correos']
        return cd

    @transaction.commit_on_success
    def save(self, *args, **kwargs):
        """  """
        cd = self.cleaned_data
        inclusionexclusion = cd['proyecto'].inclusionexclusion_set.latest('id')
        correo = Correo.objects.create(
            correos=cd['correos'],
            asunto=cd['asunto'],
            texto=cd['texto'],
            archivo_adjunto=os.path.join(
                correos_settings.ATTACHMENTS_FOLDER,
                'proyecto_%d_inclusionexclusion_%d.xls' % (
                    cd['proyecto'].id, inclusionexclusion.id)),
            user_sender=cd['user_sender'],
            proyecto=cd['proyecto'],
            inclusionexclusion=inclusionexclusion)
        return correo


class AdminInclusionExclusionForm(forms.ModelForm):
    """ inclusion exclusion form for the admin interface """

    user_sender = forms.ModelChoiceField(queryset=User.objects.all(),
                                         widget=forms.HiddenInput)
    correos = forms.CharField(
        label=u'Enviar a', required=False,
        widget=forms.TextInput(attrs={'size': 80}),
        validators=[validate_multiple_emails],
        help_text=u'Los correos deben estar separados por una coma.')
    asunto = forms.CharField(
        required=False, max_length=120,
        widget=forms.TextInput(attrs={'size': 80}))
    texto = forms.CharField(
        required=False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 15}))

    class Meta:
        model = InclusionExclusion

    def clean(self):
        """
        verifies that if the user wants to send an email, all the
        corresponding fields must be filled
        """
        cleaned_data = self.cleaned_data.copy()
        if not cleaned_data.get('correos') and (
                not cleaned_data.get('asunto') and not cleaned_data.get(
                    'texto')):
            return cleaned_data
        if cleaned_data.get('correos') and cleaned_data.get('asunto') and (
                cleaned_data.get('texto')):
            return cleaned_data
        raise forms.ValidationError(u'Para realizar el envío por correo debe \
            llenar todo los campos del fomulario.')

    @transaction.commit_on_success
    def save(self, *args, **kwargs):
        """
        saves the inclusionexclusion.
        If there is data to be sent by email, the inclusion exclusion excel
        document and the PDF document with the new contracts (if exists) are
        zipped and attached to the mail and sent to the recipients.
        Returns the inclusionexclusion object saved.
        """
        obj = super(AdminInclusionExclusionForm, self).save(*args, **kwargs)
        if self.cleaned_data.get('correos'):
            pdf = os.path.join(
                settings.MEDIA_ROOT, proyectos_settings.CONTRATOS_PDF_FOLDER,
                'contrato_proyecto_%d_inclusionexclusion_%d.pdf' % (
                    obj.proyecto.id, obj.id))
            xls = os.path.join(
                settings.MEDIA_ROOT, correos_settings.ATTACHMENTS_FOLDER,
                'proyecto_%d_inclusionexclusion_%d.xls' % (
                    obj.proyecto.id, obj.id))
            path_files_list = [xls]
            if os.path.exists(pdf):
                path_files_list.append(pdf)
            zip_files('inclusionexclusion_%d' % obj.id, path_files_list,
                      correos_settings.ATTACHMENTS_FOLDER)
            cleaned_data = self.cleaned_data
            Correo.objects.create(
                correos=cleaned_data.get('correos'),
                asunto=cleaned_data.get('asunto'),
                texto=cleaned_data.get('texto'),
                archivo_adjunto=os.path.join(
                    correos_settings.ATTACHMENTS_FOLDER,
                    'inclusionexclusion_%d.zip' % obj.id),
                user_sender=cleaned_data['user_sender'],
                proyecto=obj.proyecto,
                inclusionexclusion=obj
            )
        return obj


class AdminProyectoForm(forms.ModelForm):
    """ project form for the admin interface """

    FILES = (
        (u'pdf', u'Contratos'),
        (u'xls', u'Renovación de seguros')
    )
    user_sender = forms.ModelChoiceField(
        queryset=User.objects.all(), widget=forms.HiddenInput, required=False)
    documentos = forms.MultipleChoiceField(
        label=u'Adjuntar', required=False, choices=FILES,
        widget=forms.CheckboxSelectMultiple)
    correos = forms.CharField(
        label=u'Enviar a', required=False,
        widget=forms.TextInput(attrs={'size': 80}),
        validators=[validate_multiple_emails],
        help_text=u'Los correos deben estar separados por una coma.')
    asunto = forms.CharField(
        required=False, max_length=120,
        widget=forms.TextInput(attrs={'size': 80}))
    texto = forms.CharField(
        required=False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 15}))

    class Meta:
        model = Proyecto

    def __init__(self, *args, **kwargs):
        """
        """
        super(AdminProyectoForm, self).__init__(*args, **kwargs)
        self.fields['encargados'].widget = widgets.FilteredSelectMultiple(
            'Encargados', is_stacked=False)
        self.fields['encargados'].queryset = Encargado.objects.filter(
            activo=True)
        self.fields['aseguradoras'].widget = widgets.FilteredSelectMultiple(
            'Aseguradoras', is_stacked=False)
        self.fields['aseguradoras'].queryset = Aseguradora.objects.all()
        self.fields['descripcion'].widget = TinyMCE(
            attrs={'cols': 80, 'rows': 20})

    def clean(self):
        """
        verifies that the start date is earlier than the end date.
        Verifies that if the user wants to send an email, all the
        corresponding fields must be filled
        """
        cleaned_data = self.cleaned_data.copy()
        if cleaned_data['fecha_inicio'] >= cleaned_data['fecha_fin']:
            raise forms.ValidationError(
                u'La fecha de inicio debe ser anterior a la fecha fin.')
        if not cleaned_data.get('correos') and (
                not cleaned_data.get('asunto') and not cleaned_data.get(
                    'texto') and not cleaned_data.get('documentos')):
            return cleaned_data
        if cleaned_data.get('correos') and cleaned_data.get('asunto') and (
                cleaned_data.get('texto') and cleaned_data.get(
                    'documentos') and cleaned_data.get('user_sender')):
            return cleaned_data
        raise forms.ValidationError(u'Para realizar el envío por correo debe \
            llenar todo los campos del fomulario.')

    @transaction.commit_on_success
    def save(self, *args, **kwargs):
        """
        Saves the Proyecto object.
        If there is data to be sent by email, the selected documents are
        zipped and attached to the mail and sent to the recipients. The zip
        file is created only if it does not exist yet.
        Returns the Proyecto object saved.
        """
        obj = super(AdminProyectoForm, self).save(*args, **kwargs)
        if self.cleaned_data.get('correos'):
            path_files_list = []
            zip_name = ''
            if u'xls' in self.cleaned_data['documentos']:
                xls = os.path.join(
                    settings.MEDIA_ROOT, correos_settings.ATTACHMENTS_FOLDER,
                    'proyecto_%d.xls' % obj.id)
                path_files_list.append(xls)
            if u'pdf' in self.cleaned_data['documentos']:
                pdf = os.path.join(
                    settings.MEDIA_ROOT,
                    proyectos_settings.CONTRATOS_PDF_FOLDER,
                    'contrato_proyecto_%d.pdf' % obj.id)
                path_files_list.append(pdf)
            if len(self.cleaned_data['documentos']) == 2:
                zip_name = 'Proyecto_%d' % obj.id
            else:
                if u'xls' in self.cleaned_data['documentos']:
                    zip_name = 'proyecto_%d_xls' % obj.id
                else:
                    zip_name = 'proyecto_%d_pdf' % obj.id
            if not os.path.exists(os.path.join(
                    settings.MEDIA_ROOT, correos_settings.ATTACHMENTS_FOLDER,
                    zip_name + '.zip')):
                zip_files(zip_name, path_files_list,
                          correos_settings.ATTACHMENTS_FOLDER)
            cleaned_data = self.cleaned_data
            Correo.objects.create(
                correos=cleaned_data.get('correos'),
                asunto=cleaned_data.get('asunto'),
                texto=cleaned_data.get('texto'),
                archivo_adjunto=os.path.join(
                    correos_settings.ATTACHMENTS_FOLDER, zip_name + '.zip'),
                user_sender=cleaned_data['user_sender'],
                proyecto=obj,
            )
        return obj
