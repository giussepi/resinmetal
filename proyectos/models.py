# -*- coding: utf-8 -*-
""" proyectos models """

from django.db import models
from common.utils import highly_random_filename
from proyectos import settings as proyectos_settings
from personal.models import Personal
from datetime import date


class ModeloContrato(models.Model):
    """ stores contract models """

    PATTERNS = ('&lt;&lt;empleado_nombres&gt;&gt;',
                '&lt;&lt;empleado_dni&gt;&gt;',
                '&lt;&lt;empleado_direccion&gt;&gt;',
                '&lt;&lt;fecha_inicio&gt;&gt;',
                '&lt;&lt;fecha_fin&gt;&gt;')
    TIPO = (
        (u'p', u'personal'),
        # (u'c', u'cliente'),
    )
    titulo = models.CharField(u'Título', max_length=150)
    texto = models.TextField(
        help_text=u'Los datos o patrones que cambiarán de contrato en \
        contrato son: %s' % ', '.join(PATTERNS.__iter__()))
    tipo = models.CharField(max_length=1, choices=TIPO)

    class Meta:
        ordering = ['titulo', ]
        verbose_name = u'Modelo de Contrato'
        verbose_name_plural = u'Modelos de contrato'

    def __unicode__(self):
        return u'%s' % self.titulo


class Proyecto(models.Model):
    """ stores projects """

    nombre = models.CharField(max_length=150)
    descripcion = models.TextField()
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    monto = models.FloatField(help_text=u'Costo del proyecto')
    cliente = models.ForeignKey('clientes.Cliente')
    personal = models.ManyToManyField('personal.Personal')
    encargados = models.ManyToManyField('aseguradoras.Encargado', blank=True,
                                        null=True)
    aseguradoras = models.ManyToManyField('aseguradoras.Aseguradora',
                                          blank=True, null=True)

    class Meta:
        ordering = ['-id', ]

    def __unicode__(self):
        return u'%s' % self.nombre

    def get_initial_staff(self):
        """ returns the very first hired staff """
        first_staff = self.contrato_set.filter(
            equipo_inicial=True).values_list('personal', flat=True)
        return Personal.objects.filter(id__in=first_staff).iterator()

    def get_staff_from_inclusions_exclusions(self):
        """
        returns a list of tuples containing the staff included and excluded
        in each project inclusion exclusion
        """
        return [(inex.personal_incluido.iterator(), inex.personal_excluido.
                 iterator()) for inex in self.inclusionexclusion_set.all()]

    def get_contracts_from_inclusions_exclusions(self):
        """
        returns a list o tuple containing the inclusionexclusion object,
        an iterator over the list of contracts related to an inclusion and
        an iterator over the list of contracts related to an exclusion
        """
        return [(
            inex,
            inex.contrato_set.filter(
                personal__in=inex.personal_incluido.values_list(
                    'id', flat=True)).iterator(),
            inex.contrato_set.filter(
                personal__in=inex.personal_excluido.values_list(
                    'id', flat=True)).iterator()
        ) for inex in self.inclusionexclusion_set.all()]

    def get_final_staff(self):
        """ returns the staff the finished the project """
        first_staff = self.contrato_set.exclude(
            tipo_inclusionexclusion=u'e').values_list('personal', flat=True)
        return Personal.objects.filter(
            id__in=first_staff).iterator()

    def reactivate_staff(self):
        """
        sets the project staff available for hiring.
        Returns the number of objects affected.
        """
        personal_list = self.contrato_set.exclude(
            tipo_inclusionexclusion=u'e').values_list('personal', flat=True)
        return Personal.objects.filter(id__in=personal_list).update(
            estado=u'viable')


class InclusionExclusion(models.Model):
    """ stores inclusions exclusions """

    fecha = models.DateTimeField(auto_now_add=True)
    fecha_inicio = models.DateField(
        default=date.today,
        help_text=u'Fecha de inicio de la inclusión.')
    personal_incluido = models.ManyToManyField('personal.Personal',
                                               related_name='inclusion_set',
                                               blank=True, null=True)
    personal_excluido = models.ManyToManyField('personal.Personal',
                                               related_name='exclusion_set',
                                               blank=True, null=True)
    encargados = models.ManyToManyField('aseguradoras.Encargado')
    proyecto = models.ForeignKey(Proyecto)

    class Meta:
        ordering = ['-fecha', ]
        verbose_name = u'Inclusión Exclusión'
        verbose_name_plural = u'Inclusiones Exclusiones'

    def __unicode__(self):
        return u'%s | %s' % (self.proyecto, self.fecha.strftime(
            '%d/%m/%y | %I:%M:%S %p'))


class Contrato(models.Model):
    """ stores contracts """
    TIPO = (
        (u'p', u'Personal'),
        # (u'c', u'Cliente'),
    )
    TIPO_INCLUSIONEXCLUSION = ((u'n', u'Ninguna'),
                               (u'i', u'Incluido'),
                               (u'e', u'Excluido'))
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    monto = models.FloatField()
    texto = models.TextField()
    tipo = models.CharField(max_length=1, choices=TIPO)

    def get_path(self, filename):
        """ returns the path and filename """
        return u'%s/%s' % (proyectos_settings.CONTRATOS_ESCANEADOS_FOLDER,
                           highly_random_filename(filename))

    contrato_escaneado = models.FileField(
        upload_to=get_path, blank=True, null=True,
        help_text=u'Contrato firmado y escaneado.')
    tipo_inclusionexclusion = models.CharField(u'Tipo inclusión exclusión',
                                               max_length=1,
                                               choices=TIPO_INCLUSIONEXCLUSION,
                                               default=u'n', blank=True)
    equipo_inicial = models.BooleanField(blank=True, default=False)
    proyecto = models.ForeignKey(Proyecto)
    cliente = models.ForeignKey('clientes.Cliente', blank=True, null=True)
    personal = models.ForeignKey('personal.Personal', blank=True, null=True)
    polizas = models.ManyToManyField('aseguradoras.Poliza', blank=True,
                                     null=True)
    inclusionesexclusiones = models.ManyToManyField(
        InclusionExclusion, blank=True, null=True,
        verbose_name=u'Inclusión exclusión')

    class Meta:
        ordering = ['-fecha_inicio', 'proyecto', ]

    def __unicode__(self):
        if self.tipo == u'c':
            obj = self.cliente
        else:
            obj = self.personal
        return u'%s - %s' % (self.proyecto, obj)
