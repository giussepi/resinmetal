# -*- coding: utf-8-*-
""" Cliente Forms """

from django.db import transaction
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django import forms
from common.utils import create_username
from common.models import Distrito, Provincia, Departamento, Direccion
from clientes.models import Cliente


class AdminClienteForm(forms.ModelForm):
    """ customer form for the admin interface """
    class Meta:
        model = Cliente

    class Media:
        css = {'all': ('common/css/admin.css', )}

    id = forms.IntegerField(required=False,  widget=forms.HiddenInput)
    nombres = forms.CharField(max_length=30)
    apellidos = forms.CharField(max_length=30)
    correo = forms.EmailField(required=False)
    id_direccion = forms.IntegerField(required=False, widget=forms.HiddenInput)
    direccion = forms.CharField(max_length=250)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all())
    provincia = forms.ModelChoiceField(queryset=Provincia.objects.all())
    distrito = forms.ModelChoiceField(queryset=Distrito.objects.all())

    def __init__(self, *args, **kwargs):
        super(AdminClienteForm, self).__init__(*args, **kwargs)
        self.fields['sexo'].widget = forms.RadioSelect()
        self.fields['sexo'].choices = Cliente.SEXO
        self.fields['tipo'].widget = forms.RadioSelect()
        self.fields['tipo'].choices = Cliente.TIPO
        self.fields['direccion'].widget.attrs = {'size': 80}
        self.fields['sexo'].initial = ''

    def clean_correo(self):
        """ verifies that the email is unique for every user """
        email = self.cleaned_data.get('correo')
        if email:
            try:
                User.objects.get(email=email)
            except ObjectDoesNotExist:
                pass
            else:
                # for some reason id is not in cleaned_data ...
                id = self.data.get('id')
                if id and (email == Cliente.objects.get(id=id).user.email):
                    pass
                else:
                    raise forms.ValidationError(
                        u'Este correo ya está registrado.')
        return email

    def clean_dni(self):
        """ verifies that the dni is unique for every client """
        dni = self.cleaned_data.get('dni')
        if dni:
            try:
                Cliente.objects.get(dni=dni)
            except ObjectDoesNotExist:
                pass
            else:
                # for some reason id is not in cleaned_data ...
                id = self.data.get('id')
                if id and (dni == Cliente.objects.get(id=id).dni):
                    pass
                else:
                    raise forms.ValidationError(
                        u'Este DNI ya está registrado.')
        return dni

    def clean_ruc(self):
        """ verifies that the ruc is unique for every client """
        ruc = self.cleaned_data.get('ruc')
        if ruc:
            try:
                Cliente.objects.get(ruc=ruc)
            except ObjectDoesNotExist:
                pass
            else:
                # for some reason id is not in cleaned_data ...
                id = self.data.get('id')
                if id and (ruc == Cliente.objects.get(id=id).ruc):
                    pass
                else:
                    raise forms.ValidationError(
                        u'Este RUC ya está registrado.')
        return ruc

    @transaction.commit_on_success
    def save(self, *args, **kwargs):
        """
        creates or updates a client and creates or updates its
        corresponding user account.
        """
        cliente = super(AdminClienteForm, self).save(*args, **kwargs)
        if not cliente.user:
            obj = User.objects.create(
                username=create_username(self.cleaned_data['nombres'],
                                         self.cleaned_data['apellidos'],
                                         User),
                first_name=self.cleaned_data['nombres'],
                last_name=self.cleaned_data['apellidos'],
                email=self.cleaned_data['correo']
            )
            cliente.user = obj
            cliente.save()
            cliente.direcciones.create(
                texto=self.cleaned_data['direccion'],
                departamento=self.cleaned_data['departamento'],
                provincia=self.cleaned_data['provincia'],
                distrito=self.cleaned_data['distrito'])
        else:
            user = cliente.user
            user.first_name = self.cleaned_data['nombres']
            user.last_name = self.cleaned_data['apellidos']
            user.email = self.cleaned_data['correo']
            user.save()
            direccion = Direccion(
                id=self.cleaned_data['id_direccion'],
                texto=self.cleaned_data['direccion'],
                departamento=self.cleaned_data['departamento'],
                provincia=self.cleaned_data['provincia'],
                distrito=self.cleaned_data['distrito'])
            direccion.save()
        return cliente
