# -*- coding: utf-8 -*-
""" Clientes Admin """

from django.contrib import admin
from django.conf.urls.defaults import patterns, url
from django.core.exceptions import ObjectDoesNotExist
from common.utils import json_response
from clientes.forms import AdminClienteForm
from clientes.models import Cliente


# class ClienteTelefonosInline(admin.TabularInline):
#     """  """
#     model = Cliente.telefonos.through
#     extra = 1
#     verbose_name_plural = u'teléfonos'


# class ClienteObservacionesInline(admin.TabularInline):
#     model = Cliente.observaciones.through
#     extra = 1
#     verbose_name_plural = u'observaciones'


# class ClienteDireccionesInline(admin.TabularInline):
#     model = Cliente.direcciones.through
#     extra = 1
#     verbose_name_plural = u'direcciones'


class ClienteAdmin(admin.ModelAdmin):
    """
    admin model that manages the customer ('cliente') objects
    """
    form = AdminClienteForm
    fieldsets = (
        (u'Datos Básicos', {
            'fields': ('id', ('nombres', 'apellidos'), 'id_direccion',
                       'direccion',
                       ('departamento', 'provincia', 'distrito'),
                       'tipo', 'valoracion', ),
            'description': u'Datos necesarios para realizar un contrato.',
        }),
        (u'Datos Extra', {
            'classes': ('collapse', ),
            'fields': (('fecha_nacimiento', 'ocupacion'),
                       ('ruc', 'dni', ),
                       ('correo', 'foto'),
                       ('grado_instruccion', 'estado_civil'),
                       'sexo',
                       ),
            'description': u'Datos no extríctamente necesarios para realizar \
un contrato.',
        })
    )
    exclude = ('telefonos', 'observaciones', 'direcciones')
    list_filter = ('tipo', )
    search_fields = ('user__last_name', )

    def get_urls(self):
        urls = super(self.__class__, self).get_urls()
        my_urls = patterns(
            '',
            url(r'^ajax_get_user_data/$',
                self.admin_site.admin_view(self.ajax_get_user_data),
                name='admin_cliente_ajax_get_user_data'),
        )
        return my_urls + urls

    def ajax_get_user_data(self, request):
        """
        Gets and returns the firsname, lastname, email, and the full address
        of the client
        """
        if request.method == 'GET':
            try:
                obj = Cliente.objects.get(id=request.GET['object_id'])
            except ObjectDoesNotExist:
                data = {'response': 'error'}
            else:
                addr = obj.direcciones.all()[0]
                data = {'first_name': obj.user.first_name,
                        'last_name': obj.user.last_name,
                        'email': obj.user.email,
                        'id_direccion': addr.id,
                        'direccion': addr.texto,
                        'departamento': addr.departamento.id,
                        'provincia': addr.provincia.id,
                        'distrito': addr.distrito.id}
            return json_response(data)
        return None


admin.site.register(Cliente, ClienteAdmin)
