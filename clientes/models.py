# -*- coding: utf-8 -*-
""" Clientes Models """

from django.db import models
from common.models import BasePersona


class Cliente(BasePersona):
    """ stores the customers """

    TIPO = ((u'p', u'Persona'),
            (u'e', u'Empresa'),
            )
    tipo = models.CharField(max_length=1, choices=TIPO)

    def __unicode__(self):
        return u'%s' % (self.user.get_full_name())
