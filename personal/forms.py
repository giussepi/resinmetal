# -*- coding: utf-8 -*-
""" personal forms """

from django.db import transaction
from django.db.models import Q
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django import forms
from tinymce.widgets import TinyMCE
from common.utils import create_username
from common.models import Distrito, Provincia, Departamento, Direccion
from personal.models import Especialidad, Personal


class AdminEspecialidadForm(forms.ModelForm):
    """ Especialidad form for the admin interface """
    class Meta:
        model = Especialidad

    descripcion = forms.CharField(
        widget=TinyMCE(attrs={'cols': 80, 'rows': 30}),
        help_text=u'Ingrese una breve pero clara descripción de la \
especialidad.')


class AdminPersonalForm(forms.ModelForm):
    """ Personal form for the admin interface """
    class Meta:
        model = Personal

    class Media:
        css = {'all': ('common/css/admin.css', )}

    id = forms.IntegerField(required=False,  widget=forms.HiddenInput)
    nombres = forms.CharField(max_length=30)
    apellidos = forms.CharField(max_length=30)
    correo = forms.EmailField(required=False)
    id_direccion = forms.IntegerField(required=False, widget=forms.HiddenInput)
    direccion = forms.CharField(max_length=250)
    departamento = forms.ModelChoiceField(queryset=Departamento.objects.all())
    provincia = forms.ModelChoiceField(queryset=Provincia.objects.all())
    distrito = forms.ModelChoiceField(queryset=Distrito.objects.all())

    def __init__(self, *args, **kwargs):
        super(AdminPersonalForm, self).__init__(*args, **kwargs)
        self.fields['sexo'].widget = forms.RadioSelect()
        self.fields['sexo'].choices = Personal.SEXO
        self.fields['sistema_de_pensiones'].widget = forms.RadioSelect()
        self.fields['sistema_de_pensiones'].choices = Personal.PENSIONES
        self.fields['tipo'].widget = forms.RadioSelect()
        self.fields['tipo'].choices = Personal.TIPO
        self.fields['direccion'].widget.attrs = {'size': 80}
        self.fields['dni'].required = True
        self.fields['sexo'].required = True

    def clean_correo(self):
        """ verifies that the email is unique for every user """
        email = self.cleaned_data.get('correo')
        if email:
            try:
                User.objects.get(email=email)
            except ObjectDoesNotExist:
                pass
            else:
                # for some reason id is not in cleaned_data ...
                id = self.data.get('id')
                if id and (email == Personal.objects.get(id=id).user.email):
                    pass
                else:
                    raise forms.ValidationError(
                        u'Este correo ya está registrado.')
        return email

    def clean_dni(self):
        """ verifies that the dni is unique for every Personal object """
        dni = self.cleaned_data['dni']
        try:
            Personal.objects.get(dni=dni)
        except ObjectDoesNotExist:
            pass
        else:
            # for some reason id is not in cleaned_data ...
            id = self.data.get('id')
            if id and (dni == Personal.objects.get(id=id).dni):
                pass
            else:
                raise forms.ValidationError(
                    u'Este DNI ya está registrado.')
        return dni

    @transaction.commit_on_success
    def save(self, *args, **kwargs):
        """
        creates or updates a personal register and creates or updates its
        corresponding user account.
        """
        personal = super(AdminPersonalForm, self).save(*args, **kwargs)
        if not personal.user:
            obj = User.objects.create(
                username=create_username(self.cleaned_data['nombres'],
                                         self.cleaned_data['apellidos'],
                                         User),
                first_name=self.cleaned_data['nombres'],
                last_name=self.cleaned_data['apellidos'],
                email=self.cleaned_data['correo']
            )
            personal.user = obj
            personal.save()
            personal.direcciones.create(
                texto=self.cleaned_data['direccion'],
                departamento=self.cleaned_data['departamento'],
                provincia=self.cleaned_data['provincia'],
                distrito=self.cleaned_data['distrito'])
        else:
            user = personal.user
            user.first_name = self.cleaned_data['nombres']
            user.last_name = self.cleaned_data['apellidos']
            user.email = self.cleaned_data['correo']
            user.save()
            direccion = Direccion(
                id=self.cleaned_data['id_direccion'],
                texto=self.cleaned_data['direccion'],
                departamento=self.cleaned_data['departamento'],
                provincia=self.cleaned_data['provincia'],
                distrito=self.cleaned_data['distrito'])
            direccion.save()
        return personal


class MultiSelectEspecialidades(forms.Form):
    """ Multiselect Especialidades form for the admin interface """
    especialidad = forms.ModelMultipleChoiceField(
        queryset=Especialidad.objects.all(),
        widget=forms.CheckboxSelectMultiple(
            attrs={'class': 'custom-multiselect'}))
    experiencia_minima = forms.IntegerField(
        required=False, initial=0, help_text=u'meses mínimo',
        max_value=999, min_value=0,
        widget=forms.TextInput(attrs={'size': 1, 'autocomplete': 'off',
                                      'maxlength': 3}))

    def save(self, request):
        """
        updates the 'especialidades' session variable with the selected
        options.
        """
        request.session['especialidades'] = []
        request.session['min_experience'] = 0
        for especialidad in self.cleaned_data['especialidad']:
            request.session['especialidades'].append(especialidad.id)
        if 'experiencia_minima' in self.cleaned_data:
            request.session['min_experience'] = self.cleaned_data[
                'experiencia_minima']

    def apply_filters(self, res, request):
        """
        filters que queryset using the data stored in the session
        variable called 'especialidades', then updates the context_data
        """
        request.session['min_experience'] = request.session.get(
            'min_experience') or 0
        for especialidad in request.session.get('especialidades') or []:
            res.context_data['cl'].result_list = res.context_data[
                'cl'].result_list.filter(
                    Q(personalespecialidad__especialidad=especialidad) & Q(
                        personalespecialidad__experiencia__gte=request.session[
                            'min_experience']))
        res.context_data['cl'].queryset_list = res.context_data[
            'cl'].result_list
        res.context_data['cl'].result_count = res.context_data[
            'cl'].result_list.count()
        res.context_data['selection_note'] = u'seleccionados 0 de %d' % \
            res.context_data['cl'].result_count
        res.context_data['selection_note_all'] = u'Todos %s seleccionados' % \
            res.context_data['cl'].result_count
