//////////////////////////////////////////////////////////////////////////////
// Get user basic information (first name, last name, email and full address) 
// and put them into the corresponding objects
//////////////////////////////////////////////////////////////////////////////
(function($) {
    load_personal_user_data = function(get_URL, object_id) {
	var request = $.ajax({
	    url: get_URL,
	    type: "GET",
	    data: {object_id:object_id}, 
	    dataType: "json"
	});
	request.done(function(res, status) {
	    $('input#id_nombres').attr('value',res['first_name']);
	    $('input#id_apellidos').attr('value',res['last_name']);
	    $('input#id_correo').attr('value',res['email']);
	    $('input#id_id_direccion').attr('value',res['id_direccion']);
	    $('input#id_direccion').attr('value',res['direccion']);
	    $('select#id_departamento').children(
		'option[value='+res['departamento']+']').attr(
		    'selected','selected');
	    $('select#id_provincia').children(
		'option[value='+res['provincia']+']').attr(
		    'selected','selected');
	    $('select#id_distrito').children(
		'option[value='+res['distrito']+']').attr(
		    'selected','selected');
	});      
    }
})(jQuery);