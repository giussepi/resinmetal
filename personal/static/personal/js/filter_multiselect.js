//////////////////////////////////////////////////////////////////////////////
// sets the default django admin colors and classes for filters to the form
// makes the objects with trigger class be sent on click
// also manages the text_input object to be sent on key up only when there is
// at least one object (with trigger class) checked and there is text to be sent
//////////////////////////////////////////////////////////////////////////////
(function($) {
    
    start_multiselect = function(form, trigger, text_input){

	var any_check = false;

	manage_styles = function(){
	    $(form).attr('style','color:#999999;');
	    $(trigger).each(function(index){
		if ($(this).attr('checked')){
		    $(this).parent().attr('style','color:#5B80B2 !important;');
		    $(this).parent().parent().addClass('selected');
		    any_check = true;
		}
	    });
	    if (parseInt($(text_input).val()) > 0 & any_check){
		$(text_input).parent().attr('style',
					    'color:#5B80B2 !important;');
		$(text_input).parent().addClass('selected');
	    }
	}

	submit_form = function(){
	    $(form).submit();
	}

	manage_keyup_submit = function(){
	    if ($(this).val().length > 0){
		if (any_check)
		    submit_form();
	    }
	}

	manage_styles();
	$(trigger).click(submit_form);
	$(text_input).keyup(manage_keyup_submit);
    }

})(django.jQuery);