//////////////////////////////////////////////////////////////////////////////
// gets the address data requested and realizes the address deletion
//////////////////////////////////////////////////////////////////////////////
function update_provincias_distritos(URL_get_direccion_data, 
				     URL_update_provincia, URL_update_distrito,
				     URL_delete_direccion,
				     obj_id, obj_text,
				     obj_dept,  obj_prov, obj_dist,
				     modify_class, delete_class){

    update_selection = function(obj, new_option){
	var tmp = $(obj).children('option:selected').attr('value');
        if (tmp == new_option)
	    return true;
	$(obj).children(
	    'option[value='+new_option+']').attr(
		'selected','selected');
	$(obj).children(
	    'option[value='+tmp+']').removeAttr('selected');
    }

    update_X_selection = function(URL, obj, id){
	var request = $.ajax({
	    url: URL,
	    type: "GET",
	    data: {id:id}, 
	    dataType: "json"
        });
	request.done(function(res, status) {
	    $(obj).empty();
	    $(obj).append(res['html_code']);
	});      	
    }

    get_data = function(){
	var request = $.ajax({
	    url: URL_get_direccion_data,
	    type: "GET",
	    data: {id:$(this).attr('alt')}, 
	    dataType: "json"
        });
	request.done(function(res, status) {
	    $(obj_id).attr('value', res['id']);
	    $(obj_text).attr('value',res['texto']);
	    update_selection(obj_dept, res['departamento']);
	    update_X_selection(URL_update_provincia, obj_prov, 
			       res['provincia']);
	    update_X_selection(URL_update_distrito, obj_dist, 
			       res['distrito']);
	});      
    };

    delete_address = function(){
	var dir_id = $(this).attr('alt');
	var request = $.ajax({
	    url: URL_delete_direccion,
	    type: "POST",
	    data: {id:dir_id},
	    dataType: "json"
        });
	request.done(function(res, status) {
	    if (res['response'] == 'ok')
		$('tr#'+dir_id).hide();
	    else{
		if (res['response'] == 'last address')
		    alert('Una persona debe tener por lo menos una dirección. Si desea puede modificarla pero no borrarla.');
	    }
	});
    };

    //$(modify_class).click(get_data);
    //$(modify_class).live('click', get_data);
    $(document).on('click', modify_class, get_data);
    $(delete_class).click(delete_address);
}
