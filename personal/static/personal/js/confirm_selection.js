/////////////////////////////////////////////////////////////////////////////
// prevents the submit of the form and redirects to the provided url
//
/////////////////////////////////////////////////////////////////////////////
(function($) {

    redirection = function(obj, url){

	redirect = function(event){
	    event.preventDefault();
	    window.location.replace(url);
	};

	$(obj).click(redirect);
	
    };

})(jQuery);