/////////////////////////////////////////////////////////////////////////////
// manages everything related with the popin that appears when the contract
// model is modified
/////////////////////////////////////////////////////////////////////////////
(function($) {

    manage_model_edition = function(modified){

	submit_form = function(){
	    $('form#edit_model').submit();
	};

	submit_form_save_as = function(){
	    $('input#id_save_as').attr('value',1);
	    $('input#id_titulo').attr('value', $('input#new_titulo').val())
	    submit_form()
	};

	submit_form_update = function(){
	    $('input#id_update').attr('value',1);
	    submit_form();
	};

        if (modified == 'True')
            modal.open({
		content: "<h1>El modelo de contrato ha sido modificado</h1><fieldset class='module aligned'><div class='form-row aligned'><div><label class='required'>Título</label><input type='text' size='50' value='"+$('input#id_titulo').val()+"' name='new_titulo' id='new_titulo'></div></div></fieldset><div class='submit-row'><input type='submit' name='no_save' id='no_save' value='No guardar'><input type='submit' name='save_as' id='save_as' value='Guardar como nuevo'><input class='default' type='submit' name='update' id='update' value='Actualizar'></div>"});

	$('a#close').click(submit_form);
	$('input#no_save').click(submit_form);
	$('input#save_as').click(submit_form_save_as);
	$('input#update').click(submit_form_update);

    };

})(jQuery);