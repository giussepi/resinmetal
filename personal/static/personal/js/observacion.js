$(document).ready(function() {      

    if (typeof URL_EDIT == 'undefined'){
    	URL_EDIT = '/';
    }

    if (typeof URL_DELETE == 'undefined'){
    	URL_DELETE = '/';
    }

    $('input.changelink').click(function(event){
	event.preventDefault();
	var request = $.ajax({
            url: URL_EDIT,
            type: "GET",
            data: {id:$(this).attr('alt')}, 
            dataType: "json"
        });
	request.done(function(res, status) {
            $('textarea#id_texto').attr('value',res['texto']);
            var tipo = '0';
            if ( res['tipo'] != '1')
		tipo = '1';
            $('input#id_tipo_'+tipo).attr('checked','checked');
            $('input#id_id').attr('value',res['id']);
	});      
    });

    $('input.delete').click(function(event){
	event.preventDefault();
	var id = $(this).attr('alt')
	var request = $.ajax({
            url: URL_DELETE,
            type: "POST",
            data: {id:id}, 
            dataType: "json"
        });
	request.done(function(res, status) {
            $('tr#'+id).hide('slow');
	    $('ul#delObs').removeClass('no-display');
	});
    });

});
