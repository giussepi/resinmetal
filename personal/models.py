# -*- coding: utf-8 -*-
""" Personal Models """

from django.db import models
from django.conf import settings
from common.models import ExtendedPersona


class Especialidad(models.Model):
    """ stores the occupational specialties """
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = u'Especialidades'

    def __unicode__(self):
        return u'%s' % self.nombre


class Personal(ExtendedPersona):
    """ stores the employees """
    ESTADO = ((u'viable', u'viable'),
              (u'no_viable', u'no viable'),
              (u'contratado', u'contratado'),
              (u'nuevo', u'nuevo'),
              (u'seleccionado', u'seleccionado')
              )
    TIPO = ((u'1', u'Administrativo'),
            (u'2', u'Técnico'),
            )
    estado = models.CharField(max_length=12, choices=ESTADO, default=u'viable',
                              editable=False)
    tipo = models.CharField(max_length=1, choices=TIPO, default=u'2')
    especialidades = models.ManyToManyField(Especialidad,
                                            through='PersonalEspecialidad')

    class Meta:
        verbose_name_plural = u'Personal'

    def __unicode__(self):
        return u'%s' % (self.user.get_full_name())

    def disponibilidad(self):
        """ returns html code showing if the person is avaiable for hiring """
        if self.estado == u'viable':
            icon = u'yes'
        else:
            icon = u'no'
        return u'<img src="%sadmin/img/icon-%s.gif" alt="activo">' % (
            settings.STATIC_URL, icon)
    disponibilidad.allow_tags = True
    disponibilidad.short_description = u'Disponible'

    def delete(self):
        """
        sets the Personal object in a permanent state of non viability
        ToDo: validations before doing this
        """
        user = self.user
        user.is_active = False
        user.is_staff = False
        user.save()
        self.estado = u'no_viable'
        self.save()

    def reactivate(self):
        """
        sets the Personal object available for hire and reactivates his
        user account.
        """
        user = self.user
        user.is_active = True
        if self.tipo == u'1':
            user.is_staff = True
        user.save()
        self.estado = u'viable'
        self.save()


class PersonalEspecialidad(models.Model):
    """ stores the relations between Especialidad and Personal """
    especialidad = models.ForeignKey(
        Especialidad,
        help_text=u'Especialidad en que ha trabajado')
    personal = models.ForeignKey(Personal)
    experiencia = models.FloatField(
        default=0,
        help_text=u'Experiencia en meses')
