///////////////////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
//(function($) {

function provincias_distritos(URL_get_provincias, URL_get_distritos, 
				 obj_dept, obj_prov, obj_dist){
	update_provincia = function(){
	    if ($(this).attr('value') != ''){
		var request = $.ajax({
		    url: URL_get_provincias,
		    type: "GET",
		    data: {id:$(this).attr('value')},
		    dataType: "json"
		});
		request.done(function(res, status) {
		    $(obj_prov).empty();
		    $(obj_prov).append(res['html_code']);
		});      
	    }
	    else{
		$(obj_prov).empty();
		$(obj_prov).append(
		    '<option value="" selected="selected"> --------- </option>');
	    }
	    $(obj_dist).empty();
	    $(obj_dist).append(
		'<option value="" selected="selected"> --------- </option>');
	};

	update_distrito = function(){
	    if ($(this).attr('value') != ''){
		var request = $.ajax({
		    url: URL_get_distritos,
		    type: "GET",
		    data: {id:$(this).attr('value')}, 
		    dataType: "json"
		});
		request.done(function(res, status) {
		    $(obj_dist).empty();
		    $(obj_dist).append(res['html_code']);
		});      
	    }
	    else {
		$(obj_dist).empty();
		$(obj_dist).append(
		    '<option value="" selected="selected"> --------- </option>');
	    }
	};

	$(obj_dept).change(update_provincia);
	$(obj_prov).change(update_distrito);
    }


//})(jQuery);