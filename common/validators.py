# -*- coding: utf-8 -*-
""" Common Validators """

from django.core.exceptions import ValidationError
from django.core.validators import validate_email
import re


def validate_multiple_emails(receivers):
    """
    splits the comma separated list of emails, and verifies each one
    using the django email validator
    """
    if receivers:
        receivers = [e.strip() for e in receivers.split(',')]
        for receiver in receivers:
            try:
                validate_email(receiver)
            except ValidationError:
                raise ValidationError(u'Uno o más correos no son válidos.')


def validate_ruc(value):
    """
    validates a peruvian ruc number
    """
    try:
        int(value)
    except ValueError:
        raise ValidationError(u'Ingrese sólo los dígitos de su RUC')
    if len(value) != 11 or int(value) <= 0:
        raise ValidationError(u'Ingrese los 11 dígitos de su RUC')


def validate_dni(value):
    """
    validates a peruvian dni number
    """
    try:
        int(value)
    except ValueError:
        raise ValidationError(u'Ingrese sólo los dígitos de su DNI')
    if len(value) != 8 or int(value) <= 0:
        raise ValidationError(u'Ingrese los 8 dígitos de su DNI')


def validate_telephone(value):
    """
    validates a telephone of fifteen digits
    """
    digits = ''.join(re.findall('[0-9]+', value))
    length = len(digits)
    if 6 > length or length > 15:
        raise ValidationError(u'Ingrese un número telefónico que contenga \
entre 6 y 15 dígitos.')

def validate_username(value):
    """ validates a username """
    username_re = re.compile(r'^[\w.@+-]+$')
    if not username_re.match(value):
        raise ValidationError(_(u'"This value may contain only letters,\
        numbers and @/./+/-/_ characters."'))
