# -*- coding: utf-8 -*-
""" Common settings """

from django.conf import settings
from os import path

#DEFAULT VALUES
DEFAULT_THUMBNAIL_ALIASES = {
    'common.BasePersona.foto': {
        'preview': {'size': (100, 100), 'quality': 75, 'upscale': True, },
        'big': {'size': (300, 0), 'quality': 75, 'upscale': True, },
    },
    'common.ExtendedPersona.antecedentes_policiales': {
        'preview': {'size': (100, 100), 'quality': 75, 'upscale': True, },
        'big': {'size': (300, 0), 'quality': 75, 'upscale': True, },
    },
}

#GETTING VALUES DEFINED IN PROJECT SETTINGS
THUMBNAIL_ALIASES = getattr(
    settings, 'THUMBNAIL_ALIASES', 'DEFAULT_THUMBNAIL_ALIASES')

FOTOS_FOLDER = path.join(
    'common',
    getattr(settings, 'FOTOS_FOLDER_NAME', 'fotos')
)

ANTECEDENTES_FOLDER = path.join(
    'common',
    getattr(settings, 'ANTECEDENTES_FOLDER_NAME', 'antecedentes')
)
