# -*- coding: utf-8 -*-
""" custom template tag to get specific values from a dictionary """


from django import template


register = template.Library()


@register.assignment_tag
def get_value(dictionary, key):
    """ {% get_value dict key as val %} """
    try:
        return dictionary[key]
    except KeyError:
        return ''
