# -*- coding: utf-8 -*-
""" Common utils """

from django.conf import settings
from django.core.mail import EmailMultiAlternatives, send_mass_mail
from django.core import mail
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext, Context
from django.template.defaultfilters import slugify
from django.utils import simplejson, encoding
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from random import Random
import urllib2
import re
import unicodedata
import string
import os
import uuid
import random
import xhtml2pdf.pisa as pisa
import cStringIO as StringIO
import zipfile


def direct_response(request, *args, **kwargs):
    """
    Forma resumida de render_to_response, enviando context_instance al template
    """
    kwargs['context_instance'] = RequestContext(request)
    return render_to_response(*args, **kwargs)


def json_response(data):
    """
    Devuelve una respuesta json con la información de data
    """
    return HttpResponse(simplejson.dumps(data), mimetype='application/json')


def send_html_mail(from_email, subject, template_name, data, to_,
                   text_content='', files=None, path='email_templates/'):
    """
    Envia un correo con un template en html, data es un diccionario y files una
    lista de archivos adjuntos
    """
    ############### hack para que envie el mail con un nombre ############
    try:
        from_email = "%s <%s>" % (settings.SENDER_NAME, from_email)
    except AttributeError:
        pass
    ######################################################################
    context = Context(data)
    html_content = mark_safe(render_to_string(
        '%s%s' % (path, template_name), context))
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to_])
    msg.attach_alternative(html_content, "text/html")
    if files:
        for afile in files:
            msg.attach_file(afile)
    else:
        pass
    msg.send()

#    try:
#        context = Context(data)
#        html_content = mark_safe(render_to_string(
#            '%s%s' % (path, template_name), context))
#        msg = EmailMultiAlternatives(subject, text_content, from_email, [to_])
#        msg.attach_alternative(html_content, "text/html")
#        msg.send()
#    except:
#        a=1


def send_mass_html_mails(from_email, subject, template_name, data, receivers,
                         text_content='', files=None, path='email_templates/'):
    """
    Envía eficientemente varios correos con un template en html, data es un
    diccionario y files una lista de archivos adjuntos
    """
    ############### hack para que envie el mail con un nombre ############
    try:
        from_email = "%s <%s>" % (settings.SENDER_NAME, from_email)
    except AttributeError:
        pass
    ######################################################################
    context = Context(data)
    html_content = mark_safe(render_to_string(
        '%s%s' % (path, template_name), context))
    listado = []
    for to_ in receivers:
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to_])
        msg.attach_alternative(html_content, "text/html")
        if files:
            for afile in files:
                msg.attach_file(afile)
        else:
            pass
        listado.append(msg)
    if receivers:
        connection = mail.get_connection()
        connection.send_messages(listado)


def send_mass_text_mails(datatuple, sitio, static_url, template_name,
                         path='email_templates/'):
    """
    envia masivamente correos abriendo una sola conexión en el servidor de
    correos
    datatuple = ((subject, message, from_email, recipient_list),
    (subject, message, from_email, recipient_list),
    )
    NOTA: lo malo es que solo envia texto plano
    """
    data = {'sitio': sitio,
            'STATIC_URL': static_url,
            }
    for i in datatuple:
        data['msg'] = i[1]
        i[1] = mark_safe(
            render_to_string('%s%s' % (path, template_name), data))
    send_mass_mail(datatuple, fail_silently=False)


def get_paginated(request, object_list, num_items):
    """
    Devuelve una lista paginada de una lista de objetos y el
    número de objetos por página
    """
    paginator = Paginator(object_list, num_items)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        lista_paginada = paginator.page(page)
    except (EmptyPage, InvalidPage):
        lista_paginada = paginator.page(paginator.num_pages)
    return lista_paginada


def make_password(length=8):
    """
    Devuelve una cadena aleatoria de tamaño length
    """
    return ''.join(Random().sample(string.letters + string.digits, length))


def convert_unicode_to_string(data):
    """
    >>> convert_unicode_to_string(u'ni\xf1era')
    'niera'
    """
    return encoding.smart_str(data, encoding='ascii', errors='ignore')


def strip_accents(data):
    """
    retira los acentos presentes en un unicode
    """
    return ''.join((c for c in unicodedata.normalize('NFD\
', data) if unicodedata.category(c) != 'Mn'))


def get_data_list(url, reg_exp1, reg_exp2):
    """
    obtiene una lista de datos de la url dada, siguiendo las 2 expresiones
    regulares dadas
    """
    response = urllib2.urlopen(url)
    page = response.read()

    seg1 = re.findall(reg_exp1, page, re.DOTALL)
    data_list = []
    tmp = []
    for seg2 in seg1:
        tmp = re.findall(reg_exp2, seg2)
        try:
            tmp.remove('TODO')
        except ValueError:
            pass
        data_list.extend(tmp)
    return data_list


def get_departamentos():
    """retorna todos los 25 departamentos"""
    return get_data_list(
        'http://iinei.inei.gob.pe/iinei/siscodes/ubigeobuscar.asp?seldpto=00&\
selanios=2010', '<select id="select2".*?>(.*?)</select>',
        '<option value="(.*?)".*?>(.*?)</option>',
    )


def get_provincias_from(departamento_id):
    """retorna las provincias de un departamento(01-25)"""
    return get_data_list(
        'http://iinei.inei.gob.pe/iinei/siscodes/ubigeobuscar.asp?seldpto=%s&\
selanios=2010' % departamento_id,
        '<select id="select3".*?>(.*?)</select>',
        '<option value="(.*?)".*?>(.*?)</option>'
    )


def get_distritos_from(departamento_id, provincia_id):
    """retorna los distritos de una provincia"""
    return get_data_list(
        'http://iinei.inei.gob.pe/iinei/siscodes/ubigeobuscar.asp?seldpto=%s&\
selanios=2010' % departamento_id,
        "if \(indice=='%s'\){(.*?)}" % provincia_id,
        "document.frmBusca.select4.options.*?text='(.*?)';"
    )


def get_data_from_inei():
    """
    Obtiene todos los departamentos, provincias y distritas del INEI y los
    guarda en sus tablas correspondientes
    """
    from common.models import Departamento
    for departamento in get_departamentos():
        depa = Departamento.objects.create(
            nombre=convert_unicode_to_string(departamento[1]))
        for provincia in get_provincias_from(departamento[0]):
            # prov = Provincia.objects.create(
            #     nombre=convert_unicode_to_string(provincia[1]),
            #     departamento=departamento)
            prov = depa.provincia_set.create(
                nombre=convert_unicode_to_string(provincia[1]))
            for dist in get_distritos_from(departamento[0], provincia[0]):
                #istrito.objects.create(nombre=convert_unicode_to_string(dist),
                #                         provincia=prov)
                prov.distrito_set.create(
                    nombre=convert_unicode_to_string(dist))
    print 'Departamentos, provincias y distritos creados'


def json_fastsearch(queryset, search_field, substring, fields):
    """
    Realiza una fastsearch dentro de una tabla retornando un diccionario json
    con los atributos seleccionados enviando los nombres correctos en forma de
    diccionario con el siguiente formato
    {"id": "id",
    "name": "nombre",
    "description": "descripcion",
    "image": "foto"}
    """
    filter_ = "%s__icontains" % search_field
    match_list = queryset.filter(**{filter_: substring})
    json_dict = []
    for obj in match_list:
        json_dict.append(
            {"id": getattr(obj, fields["id"]),
             "name": getattr(obj, fields["name"]),
             "subtitle": getattr(obj, fields["subtitle"]).nombre,
             "description": getattr(obj, fields["description"]),
             "image": getattr(obj, fields["image"]).extra_thumbnails.get('\
small').absolute_url,
             # ToDo: Para athumbs
             # "image": getattr(obj, fields["image"]).generate_url("icon")
             })

    return json_response(json_dict)


def get_object_or_none(model, *args, **kwargs):
    """
    Retorna el objeto o None en caso de que no exista este
    """
    try:
        model.objects.get(*args, **kwargs)
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None


def random_string_with_length(longitud=10):
    """
    returns a random string with a defined length
    """
    lista_letras_numeros = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNO\
PQRSTUVWXYZ1234567890"
    return "".join(
        [random.choice(lista_letras_numeros) for i in xrange(longitud)])


def highly_random_filename(filename, longitud=20):
    """
    retorna un nombre aleatorio de longitud igual a 'longitud',
    manteniendo la extension del archivo
    """
    root, ext = os.path.splitext(filename)
    return random_string_with_length(longitud) + ext


def create_username(first_name, last_name, model=None):
    """
    creates a username based on first_name and last_name parameters making sure
    that it is unique for the model typed (if there is one)
    """
    firstname = convert_unicode_to_string(first_name).split()
    lastname = convert_unicode_to_string(last_name).split()
    username = firstname[0] + lastname[0] + random_string_with_length(5)
    if model:
        unique_username = False
        while not unique_username:
            try:
                model.objects.get(username=username)
            except ObjectDoesNotExist:
                unique_username = True
            else:
                username = firstname[0] + lastname[
                    0] + random_string_with_length(5)
    return username


def create_pdf(filename, text, path='', obj=None):
    """
    if there is no path provided creates one by default
    Verifies that the new path exists, it doesn't exist, creates the
    folders.
    Creates a pdf with the provided text and filename, the document is saved
    using the provided path.
    If the object instance is provided, it mush have a pdf attribute to save
    the pdf url
        pdf = models.CharField('pdf', max_length=150, blank=True)
    """
    if not path:
        path = os.path.join(settings.MEDIA_ROOT, 'common/pdf')
    if not os.path.exists(path):
        os.makedirs(path)
    myfile = StringIO.StringIO()
    pdf = pisa.CreatePDF(text, myfile)
    new_file = open(os.path.join(path, filename), 'wb')
    new_file.write(myfile.getvalue())
    new_file.close()
    myfile.close()

    if obj:
        obj.pdf = os.path.join(settings.MEDIA_URL, path, filename)
        obj.save()


def return_pdf(text, filename='document'):
    """
    Creates and returns a pdf document with the provided text and filename
    Before creating the pdf strips the accents of the file name.
    """
    myfile = StringIO.StringIO()
    pdf = pisa.CreatePDF(text, myfile)
    myfile.seek(0)
    # myfile.close()
    response = HttpResponse(myfile, mimetype='application/pdf')
    response['Content-Disposition'] = 'attachment; \
    filename="%s.pdf"' % strip_accents(filename)
    return response


def create_xls(base_template_path, new_xls_name, sheet_index):
    """
     sheet_index: 0  based
     aca abria que darle lso datos listos para poner
     REVISAR LOS EJEMPLOS Y LA DOCUMENTACION, DERRE HAY ALGO MUY BUENO
     YA IMPLEMENTADO
     para q sea rapido utilizar los metodos de tiposs de celda
     """
    from xlrd import open_workbook
    #from xlwt import easyxf
    from xlutils.copy import copy

    rb = open_workbook(base_template_path, formatting_info=True)
    wb = copy(rb)
    ws = wb.get_sheet(sheet_index)
    for i in xrange(1, 21):
        #ws.write(i, 0, 'insersion %d' % i)
        ws.row(i).set_cell_text(0, 'insersion %d' % i)
    wb.save(new_xls_name)


def zip_files(filename, path_files_list=None, save_path='zip_files',
              zip_deflated=True, allowzip64=False):
    """
    if the path_files_list is an empty list:
      * Returns False.
    else:
      * Concatenates de MEDIA_ROOT with the save_path, if this new path does
        not exist, it is created.
      * If zip_deflated is True, sets the compression to ZIP_DEFLATED; else,
        sets the compression to ZIP_STORED
      * Creates a zip file with the filename provided, then adds the files to
        the zip file only if the file exists.
      * Returns True
    """
    if not path_files_list:
        return False
    save_path = os.path.join(settings.MEDIA_ROOT, save_path)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    if zip_deflated:
        compression = zipfile.ZIP_DEFLATED
    else:
        compression = zipfile.ZIP_STORED
    zip_file = zipfile.ZipFile(os.path.join(save_path, filename) + '.zip', 'w',
                               compression, allowzip64)
    for path in path_files_list:
        if os.path.exists(path):
            zip_file.write(path, os.path.basename(path))
    zip_file.close()
    return True


def unique_random_string(model, field, string_length=10):
    """
    returns a unique string for the model field.
    """
    unique = False
    while (not unique):
        random_str = random_string_with_length(string_length)
        try:
            model.objects.get(**{'{0}'.format(field): random_str})
        except ObjectDoesNotExist:
            unique = True
    return random_str


def unique_slug(model, slug_field, data):
    """  """
    unique = False
    uslug = slugify(data)
    ucode = 0
    while (not unique):
        try:
            model.objects.get(**{'{0}'.format(slug_field): uslug})
        except ObjectDoesNotExist:
            unique = True
        else:
            uslug += str(ucode)
            ucode += 1
    return uslug

def get_file_path(instance, filename, to='images/'):
    """
    This function generate filename with uuid4
    it's useful if:
    - you don't want to allow others to see original uploaded filenames
    - users can upload images with unicode in filenames wich can confuse
    browsers and filesystem
    """
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(to, filename)
