# -*- coding: utf-8 -*-
""" Common Models """

from django.db import models
from django.contrib.auth.models import User
from easy_thumbnails.fields import ThumbnailerImageField
from validators import validate_ruc, validate_dni, validate_telephone
from common.utils import highly_random_filename
from common import settings as common_settings


class BaseData(models.Model):
    """ abstract class containing the most basic data """

    VALORACION = (
        (1, u'Malo'),
        (2, u'Regular'),
        (3, u'Bueno'),
        (4, u'Muy bueno'),
        (5, u'Excelente'),
    )
    ruc = models.CharField(max_length=11, blank=True,
                           validators=[validate_ruc])
    web = models.URLField(blank=True)
    valoracion = models.IntegerField(default=2, choices=VALORACION)
    telefonos = models.ManyToManyField(
        'Telefono',
        related_name="%(app_label)s_%(class)s_related",
        blank=True, null=True)
    observaciones = models.ManyToManyField(
        'Observacion',
        related_name="%(app_label)s_%(class)s_related",
        blank=True, null=True)

    class Meta:
        abstract = True


class BasePersona(BaseData):
    """ abstract class containing the basic data for a person """

    SEXO = (
        (u'M', u'Masculino'),
        (u'F', u'Femenino'),
    )
    INSTRUCCION = (
        (u'p', u'Primaria'),
        (u's', u'Secundaria'),
        (u't', u'Técnica'),
        (u'u', u'Universitaria'),
        (u'm', u'Maestría'),
        (u'd', u'Doctorado'),
    )
    ESTADO_CIVIL = (
        (u'sol', u'Soltero'),
        (u'cas', u'Casado'),
        (u'div', u'Divorciado'),
        (u'con', u'Conviviente'),
        (u'viu', u'Viudo'),
    )
    dni = models.CharField(
        blank=True, max_length=8, validators=[validate_dni, ])
    #nacionalidad
    sexo = models.CharField(
        blank=True, max_length=1, choices=SEXO, default=u'M')
    grado_instruccion = models.CharField(
        blank=True, max_length=1, choices=INSTRUCCION)
    estado_civil = models.CharField(
        blank=True, max_length=3, choices=ESTADO_CIVIL)
    ocupacion = models.CharField(blank=True, max_length=100)

    def get_path(self, filename):
        """ returns the path and the filename """
        return u'%s/%s' % (common_settings.FOTOS_FOLDER,
                           highly_random_filename(filename))

    foto = ThumbnailerImageField(
        upload_to=get_path, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    direcciones = models.ManyToManyField(
        'Direccion', blank=True, null=True,
        related_name="%(app_label)s_%(class)s_related")
    user = models.ForeignKey(User, blank=True, null=True)

    class Meta:
        abstract = True


class ExtendedPersona(BasePersona):
    """ abstract class containing the data for a person """

    PENSIONES = (
        (u'snp', u'Sistema Nacional de Pensiones'),
        (u'afp', u'Administrador de Fondos de Pensiones'),
    )
    GRUPO_SANGUINEO = (
        (u'O+', u'O+'),
        (u'O-', u'O-'),
        (u'A+', u'A+'),
        (u'A-', u'A-'),
        (u'B+', u'B+'),
        (u'B-', u'B-'),
        (u'AB+', u'AB+'),
        (u'AB-', u'AB-'),
    )
    sistema_de_pensiones = models.CharField(max_length=3, choices=PENSIONES,
                                            blank=True)
    nro_cuspp = models.CharField(blank=True, max_length=20,
                                 help_text=u'Código Único de Identificación \
del Sistema Privado de Pensiones')
    nro_seguro_social = models.CharField(blank=True, max_length=20)
    grupo_sanguineo = models.CharField(u'Grupo Sanguíneo',
                                       max_length=3,
                                       choices=GRUPO_SANGUINEO)

    def get_path(self, filename):
        return u'%s/%s' % (common_settings.ANTECEDENTES_FOLDER,
                           highly_random_filename(filename))

    antecedentes_policiales = ThumbnailerImageField(
        blank=True, null=True,
        upload_to=get_path)

    class Meta:
        abstract = True


class Observacion(models.Model):
    """ stores the observations (positives and negatives) """
    TIPO = (
        (1, u'Buena'),
        (-1, u'Mala'),
    )
    fecha = models.DateTimeField(auto_now_add=True)
    texto = models.TextField()
    tipo = models.IntegerField(choices=TIPO)

    class Meta:
        ordering = ['-fecha', ]
        verbose_name = u'Observación'
        verbose_name_plural = u'Observaciones'

    def __unicode__(self):
        return u'%s' % self.fecha.strftime('%d/%m/%y %I:%M:%S %p')


class TipoTelefono(models.Model):
    """ stores the telephone type """
    nombre = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        ordering = ['nombre', ]
        verbose_name = u"Tipo de teléfono"
        verbose_name_plural = u"Tipos de teléfono"


class Telefono(models.Model):
    """ stores telephones """
    numero = models.CharField(u"número", max_length=20,
                              validators=[validate_telephone, ])
    tipo = models.ForeignKey(TipoTelefono)

    def __unicode__(self):
        return u'%s' % self.numero

    class Meta:
        verbose_name = u"Teléfono"
        verbose_name_plural = u"Teléfonos"


class Departamento(models.Model):
    """ stores departments """
    nombre = models.CharField(max_length=128)

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        ordering = ['nombre', ]


class Provincia(models.Model):
    """ stores provinces """
    nombre = models.CharField(max_length=128)
    departamento = models.ForeignKey(Departamento)

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        ordering = ['nombre', ]


class Distrito(models.Model):
    """ stores districts """
    nombre = models.CharField(max_length=128)
    provincia = models.ForeignKey(Provincia)

    def __unicode__(self):
        return unicode(self.nombre)

    class Meta:
        ordering = ['nombre', ]


class Direccion(models.Model):
    """ stores addresses """
    texto = models.CharField(max_length=250)
    distrito = models.ForeignKey(Distrito)
    provincia = models.ForeignKey(Provincia)
    departamento = models.ForeignKey(Departamento)

    def __unicode__(self):
        return u'%s' % self.texto

    def full_address(self):
        """ returns the full address """
        return u'%s, perteneciente al distrito de %s, provincia de %s del \
        departamento de %s' % (self.texto, self.distrito, self.provincia,
                               self.departamento)
