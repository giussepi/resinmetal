#-*- coding: utf-8 -*-
""" Common Forms """

from django import forms
from django.forms.formsets import BaseFormSet, formset_factory
from django.shortcuts import get_object_or_404
from models import Telefono, Observacion, Direccion


class AdminTelefonoForm(forms.ModelForm):
    """ telephone form for the admin interface """

    class Meta:
        model = Telefono

    def saveTo(self, obj):
        """
        graba y añade el telefono a la lista de teléfonos
        requiere que el related_name a telefono sea telefonos
        """
        telephone = self.save()
        obj.telefonos.add(telephone)
        return telephone


class AdminObservacionForm(forms.ModelForm):
    """ Observation form for the admin interface """

    id = forms.IntegerField(required=False,  widget=forms.HiddenInput)

    class Meta:
        model = Observacion

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.fields['tipo'].widget = forms.RadioSelect()
        self.fields['tipo'].choices = Observacion.TIPO

    def clean(self):
        """
        Verifies that the id belongs to an existing object.
        If don't, deletes the id from cleaned data to saves de observation
        as a new object.
        """
        cd = super(self.__class__, self).clean()
        id = cd.get('id')
        if id:
            try:
                Observacion.objects.get(id=id)
            except Observacion.DoesNotExist:
                del cd['id']
        return cd

    def saveTo(self, m2m_field):
        """ updates or saves an observacion object """
        cd = self.cleaned_data
        id = cd.get('id')
        if id:
            obs = Observacion.objects.get(id=id)
            obs.tipo = cd['tipo']
            obs.texto = cd['texto']
            obs.save()
        else:
            obs = self.save()
            m2m_field.add(obs)
        return self.__class__()


class BaseAdminXFormSet(BaseFormSet):
    """ base formset that defines the save method to be used by the formset """

    def save(self, obj):
        """ saves all the non empty forms """
        for form in self.forms:
            if form.has_changed():
                form.saveTo(obj)


AdminFonoFormSet = formset_factory(AdminTelefonoForm,
                                   formset=BaseAdminXFormSet,
                                   extra=1, max_num=5)


class AdminDeleteTelefonoForm(forms.Form):
    """ Deletes the telephone object """

    id = forms.IntegerField(widget=forms.HiddenInput)

    def save(self):
        """ deletes the telephone """
        telephone = get_object_or_404(Telefono, id=self.cleaned_data['id'])
        return telephone.delete()


# class AdminDeleteObservacionForm(forms.Form):
#     """  Deletes and observacion object  """

#     id = forms.IntegerField(widget=forms.HiddenInput)

#     def clean_id(self):
#         try:
#             obs = Observacion.objects.get(id=self.cleaned_data['id'])
#         except Observacion.DoesNotExist:
#             raise forms.ValidationError(u'Id incorreto')
#         return obs

#     def save(self):
#         obs = Observacion.objects.get(id=self.cleaned_data['id'])
#         return obs.delete()

class AdminDireccionForm(forms.ModelForm):
    """ Address form for the admin interface """
    id = forms.IntegerField(required=False,  widget=forms.HiddenInput)

    class Meta:
        model = Direccion
        fields = ('id', 'texto', 'departamento', 'provincia', 'distrito', )

    def clean(self):
        """
        Verifies that the id belongs to an existing object.

        If don't, deletes the id from cleaned data to saves the address
        as a new object.
        """
        cd = super(self.__class__, self).clean()
        id = cd.get('id')
        if id:
            try:
                Direccion.objects.get(id=id)
            except Direccion.DoesNotExist:
                del cd['id']
        return cd

    def saveTo(self, m2m_field):
        """ updates or saves an direccion object """
        cd = self.cleaned_data
        id = cd.get('id')
        if id:
            obj = Direccion.objects.get(id=id)
            obj.texto = cd['texto']
            obj.departamento = cd['departamento']
            obj.provincia = cd['provincia']
            obj.distrito = cd['distrito']
            obj.save()
        else:
            obj = self.save()
            m2m_field.add(obj)
        return self.__class__()
